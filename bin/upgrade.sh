echo "Applying system configuration..."
pushd ~/cfg > /dev/null
nixos-rebuild switch --use-remote-sudo --impure --flake .#
popd > /dev/null

