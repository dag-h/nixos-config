pushd ~/cfg > /dev/null
nix registry remove nixpkgs
nix flake update
nix registry pin nixpkgs
popd > /dev/null
