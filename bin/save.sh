echo "Saving changes..."
pushd ~/cfg > /dev/null
git diff
git add .
git commit
git pull --rebase
git push && echo "Changes successfully saved."
