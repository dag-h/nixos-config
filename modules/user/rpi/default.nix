{ pkgs, config, lib, ... }:
with lib;

let
  cfg = config.dag.rpi;
in {
  options.dag.rpi = {
    enable = mkEnableOption "Enable Raspberry Pi home configuration";
  };

  config = mkIf (cfg.enable) {
    home.packages = with pkgs; [
      ripgrep
      fd
      bat
      eza
      unzip
      nix-index
    ];
  };
}
