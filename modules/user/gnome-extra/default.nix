{ pkgs, config, lib, ... }:
with lib;

let cfg = config.dag.gnome-extra;
in {
  options.dag.gnome-extra = {
    enable = mkOption {
      description = "Enable some Gnome extensions and apply dconf settings";
      type = types.bool;
      default = false;
    };
  };

  config = mkIf (cfg.enable) {
    home.packages = with pkgs;
      # gnome packages
      (with pkgs.gnome; [
        zenity
        gnome-tweaks

      # gnome extensions
      ]) ++ (with pkgs.gnomeExtensions; [
        appindicator
        blur-my-shell
        just-perfection
        gsconnect
      ]);

    xsession = {
      enable = true;
      initExtra = ''
        ${pkgs.xorg.xinput.pname} --set-prop "Kensington Expert Wireless TB Mouse" 295 0, 0, 1
        ${pkgs.xorg.xinput.pname} --set-prop "Kensington Expert Wireless TB Mouse" 299 0
        ${pkgs.xorg.xinput.pname} --set-button-map "Kensington Expert Wireless TB Mouse" 1 2 8 4 5 6 7 3 9
        ${pkgs.xorg.xinput.pname} --set-prop "Kensington Expert Wireless TB Mouse" 297 3
      '';
    };

    gtk = {
      enable = true;
      gtk4.extraCss = ''
        vte-terminal {
          padding: 20px 20px 20px 20px;
        }
      '';
      cursorTheme = {
        name = "Adwaita";
      };
    };

    home.sessionVariables = {
      XCURSOR_THEME = "Adwaita";
    };

    dconf.settings = with lib.hm.gvariant; let
      modKey = "Super";
    in {
      "org/gnome/desktop/calendar" = { show-weekdate = true; };

      "org/gnome/desktop/input-sources" = {
        per-window = false;
        sources = [ (mkTuple [ "xkb" "se" ]) (mkTuple [ "xkb" "us" ]) (mkTuple [ "xkb" "eu" ]) ];
        xkb-options = [ "eurosign:e" "lv3:ralt_switch" "caps:ctrl_modifier" ];
      };

      "org/gnome/desktop/interface" = {
        clock-show-seconds = false;
        clock-show-weekday = true;
        enable-animations = true;
        enable-hot-corners = false;
        font-antialiasing = "grayscale";
        font-hinting = "full";
        gtk-im-module = "gtk-im-context-simple";
        gtk-theme = "Adwaita";
        locate-pointer = false;
        show-battery-percentage = true;
        cursor-theme = "Adwaita";
      };

      "org/gnome/desktop/peripherals/mouse" = { accel-profile = "flat"; };

      "org/gnome/desktop/peripherals/touchpad" = {
        two-finger-scrolling-enabled = true;
      };

      "org/gnome/desktop/peripherals/trackball" = {
        scroll-wheel-emulation-button = 8;
      };

      "org/gnome/desktop/wm/keybindings" = {
        close = [ "<${modKey}>q" ];
        cycle-windows = [ "<${modKey}>space" ];
        cycle-windows-backward = [ "<Shift><${modKey}>space" ];
        move-to-monitor-left = [ "<${modKey}>comma" ];
        move-to-monitor-right = [ "<${modKey}>period" ];
        move-to-workspace-1 = [ "<${modKey}><Shift>1" ];
        move-to-workspace-2 = [ "<${modKey}><Shift>2" ];
        move-to-workspace-3 = [ "<${modKey}><Shift>3" ];
        move-to-workspace-4 = [ "<${modKey}><Shift>4" ];
        move-to-workspace-5 = [ "<${modKey}><Shift>5" ];
        move-to-workspace-6 = [ "<${modKey}><Shift>6" ];
        move-to-workspace-7 = [ "<${modKey}><Shift>7" ];
        move-to-workspace-8 = [ "<${modKey}><Shift>8" ];
        move-to-workspace-9 = [ "<${modKey}><Shift>9" ];
        panel-run-dialog = [ "<${modKey}>d" ];
        switch-to-workspace-1 = [ "<${modKey}>1" ];
        switch-to-workspace-2 = [ "<${modKey}>2" ];
        switch-to-workspace-3 = [ "<${modKey}>3" ];
        switch-to-workspace-4 = [ "<${modKey}>4" ];
        switch-to-workspace-5 = [ "<${modKey}>5" ];
        switch-to-workspace-6 = [ "<${modKey}>6" ];
        switch-to-workspace-7 = [ "<${modKey}>7" ];
        switch-to-workspace-8 = [ "<${modKey}>8" ];
        switch-to-workspace-9 = [ "<${modKey}>9" ];
        toggle-fullscreen = [ "<Shift><${modKey}>f" ];
        toggle-maximized = [ "<${modKey}>f" ];
        toggle-on-all-workspaces = [ "<${modKey}>s" ];
        unmaximize = [ "<${modKey}>r" ];
        switch-input-source = [ "<${modKey}>i" ];
        minimize = [];
      };

      "org/gnome/desktop/wm/preferences" = {
        action-middle-click-titlebar = "none";
        button-layout = ":";
        focus-mode = "click";
        mouse-button-modifier = "<${modKey}>";
        num-workspaces = 9;
        resize-with-right-button = true;
        workspace-names =
          [ "1" "2" "3" "4" "5" "6" "7" "8" "9" ];
      };

      "org/gnome/mutter" = {
        attach-modal-dialogs = true;
        center-new-windows = true;
        dynamic-workspaces = false;
        edge-tiling = true;
        focus-change-on-pointer-rest = true;
        workspaces-only-on-primary = true;
      };

      "org/gnome/mutter/keybindings" = {
        toggle-tiled-left = [ "<${modKey}>h" ];
        toggle-tiled-right = [ "<${modKey}>l" ];
      };

      "org/gnome/nautilus/icon-view" = { default-zoom-level = "large"; };

      "org/gnome/nautilus/list-view" = { use-tree-view = false; };

      "org/gnome/nautilus/preferences" = {
        default-folder-viewer = "icon-view";
        migrated-gtk-settings = true;
        search-filter-time-type = "last_modified";
        search-view = "list-view";
        show-delete-permanently = false;
      };

      "org/gnome/settings-daemon/plugins/color" = {
        night-light-enabled = true;
      };

      "org/gnome/settings-daemon/plugins/media-keys" = {
        custom-keybindings = [
          "/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/"
          "/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom1/"
          "/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom2/"
          "/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom3/"
          "/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom4/"
          "/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom5/"
          "/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom6/"
        ];
      };

      "org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0" =
        {
          binding = "<${modKey}>Return";
          command = "kgx";
          name = "terminal";
        };

      "org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom1" =
        {
          binding = "<${modKey}>w";
          command = "firefox";
          name = "browser";
        };

      "org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom2" =
        {
          binding = "<${modKey}>e";
          command = "emacsclient -c -n";
          name = "emacs";
        };

      "org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom3" =
        {
          binding = "<${modKey}>Home";
          command = "gnome-calendar";
          name = "calendar";
        };

      "org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom4" =
        {
          binding = "<${modKey}>n";
          command = "neovide";
          name = "neovide";
        };

      "org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom5" =
        {
          binding = "<${modKey}>b";
          command = "nautilus";
          name = "files";
        };

      "org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom6" =
        {
          binding = "<${modKey}>o";
          command = "obsidian";
          name = "obsidian";
        };

      "org/gnome/settings-daemon/plugins/power" = {
        power-button-action = "suspend";
        sleep-inactive-ac-type = "nothing";
      };

      "org/gnome/shell" = {
        disable-user-extensions = false;
        enabled-extensions = [
          "appindicatorsupport@rgcjonas.gmail.com"
          "blur-my-shell@aunetx"
          "gsconnect@andyholmes.github.io"
          "just-perfection-desktop@just-perfection"
        ];
        last-selected-power-profile = "power-saver";
        welcome-dialog-last-shown-version = "40.1";
      };

      "org/gnome/shell/extensions/appindicator" = {
        custom-icons = "@a(sss) []";
        icon-brightness = 0.0;
        icon-contrast = 0.0;
        icon-saturation = 1.0;
        icon-size = 18;
        tray-pos = "right";
      };

      "org/gnome/shell/extensions/blur-my-shell" = {
        appfolder-dialog-opacity = 0.12;
        blur-dash = true;
        brightness = 0.24;
        dash-opacity = 0.12;
        debug = false;
        hidetopbar = false;
        sigma = 52;
      };

      "org/gnome/shell/extensions/blur-my-shell/panel" = {
        blur = false;
      };

      "org/gnome/shell/extensions/gsconnect" = {
        devices = [ "E270A56204934FCDA149F0918D90345A" ];
        enabled = true;
        id = "cc8cb8cb-4d27-4352-8746-44b3429792a5";
        name = "compro";
      };

      "org/gnome/shell/extensions/gsconnect/device/E270A56204934FCDA149F0918D90345A" =
        {
          certificate-pem = ''
            -----BEGIN CERTIFICATE-----
            MIIDEjCCAfoCAQowDQYJKoZIhvcNAQEEBQAwTzEUMBIGA1UECwwLS2RlIGNvbm5l
            Y3QxDDAKBgNVBAoMA0tERTEpMCcGA1UEAwwgRTI3MEE1NjIwNDkzNEZDREExNDlG
            MDkxOEQ5MDM0NUEwHhcNMjIxMDExMTI0ODQ3WhcNMjMxMDExMTI0ODQ3WjBPMRQw
            EgYDVQQLDAtLZGUgY29ubmVjdDEMMAoGA1UECgwDS0RFMSkwJwYDVQQDDCBFMjcw
            QTU2MjA0OTM0RkNEQTE0OUYwOTE4RDkwMzQ1QTCCASIwDQYJKoZIhvcNAQEBBQAD
            ggEPADCCAQoCggEBAKe+6Z6SADluJ9vIGUg2dMkuhsO4nlvKRp79MUSZgTb7bFe7
            0PGrBvL4YGGFUCjxPCAVdfWD9E3LpL9cuirbKTbO2342Kv9oti2Lly4gGij0cO39
            bxODXATlUeWPBUHmQ3Y9fmlbTYqEkaJSvYGUKpR4FSvcA63AVnXcrJquBwruXsG6
            4MYwowH9tNSgeefSmTtqwwJ2HONsN0N0YO6hMFVswzYSuQS9PTmkfQFHPOPznpgR
            YHnj1tyuod8mJFU4MVHq8Q391kjtaLQpAzd/i6H26+bjziMGK/OsJieJuPVzHJtm
            kDsidzJSaapPVlAa0WXK0QpztUTqQBK9QONvLlsCAwEAATANBgkqhkiG9w0BAQQF
            AAOCAQEAnmM3fT3MFikCHE2Jm+OPHxrRZ3/k+uTcSo15eke3BUAl0E/waq1ps7S6
            YdTbhX4mXp+cDCJPWBpEF+3ol92zacNZOS5tzCIsS3cMb7mjQ1jD6yt7BnuPA6uv
            VQc4+qgvRGYffD7WdnCnZjz0BAwRQyujE/9Z/14RFb6UjKcesKkmNGNzCwQZpOlC
            pt4eWPtQGiXyozG4NIKgJhJAjOEUozvPiNgFUDdGXNjGBUvObZXAMCRrDOhxXltU
            L61qG7kwaf0M+hFTa/CTOiDmIFuns7kNpk3/tUF4TT8hIg0hbkN9rs3VovQxLGd8
            UCL7ezLtCYWh5qcvOY+18vo9oSMWxw==
            -----END CERTIFICATE-----
          '';
          incoming-capabilities = [
            "kdeconnect.battery"
            "kdeconnect.battery.request"
            "kdeconnect.clipboard"
            "kdeconnect.clipboard.connect"
            "kdeconnect.findmyphone.request"
            "kdeconnect.ping"
            "kdeconnect.runcommand"
            "kdeconnect.share.request"
          ];
          last-connection = "lan://192.168.1.89:1717";
          name = "iPhone";
          outgoing-capabilities = [
            "kdeconnect.battery"
            "kdeconnect.battery.request"
            "kdeconnect.clipboard"
            "kdeconnect.clipboard.connect"
            "kdeconnect.findmyphone.request"
            "kdeconnect.mousepad.request"
            "kdeconnect.ping"
            "kdeconnect.presenter"
            "kdeconnect.runcommand.request"
            "kdeconnect.share.request"
          ];
          paired = true;
          supported-plugins = [
            "battery"
            "clipboard"
            "findmyphone"
            "mousepad"
            "ping"
            "presenter"
            "runcommand"
            "share"
          ];
          type = "phone";
        };

      "org/gnome/shell/extensions/gsconnect/device/E270A56204934FCDA149F0918D90345A/plugin/battery" =
        {
          custom-battery-notification-value = mkUint32 80;
        };

      "org/gnome/shell/extensions/gsconnect/device/E270A56204934FCDA149F0918D90345A/plugin/notification" =
        {
          applications = ''
            {"Printers":{"iconName":"org.gnome.Settings-printers-symbolic","enabled":true},"Nicotine+":{"iconName":"org.nicotine_plus.Nicotine","enabled":true},"Evolution Alarm Notify":{"iconName":"appointment-soon","enabled":true},"EasyEffects":{"iconName":"com.github.wwmm.easyeffects","enabled":true},"Date & Time":{"iconName":"org.gnome.Settings-time-symbolic","enabled":true},"Lutris":{"iconName":"lutris","enabled":true},"Disk Usage Analyzer":{"iconName":"org.gnome.baobab","enabled":true},"Geary":{"iconName":"org.gnome.Geary","enabled":true},"Power":{"iconName":"org.gnome.Settings-power-symbolic","enabled":true},"Console":{"iconName":"org.gnome.Console","enabled":true},"Color":{"iconName":"org.gnome.Settings-color-symbolic","enabled":true},"Files":{"iconName":"org.gnome.Nautilus","enabled":true},"Clocks":{"iconName":"org.gnome.clocks","enabled":true},"Archive Manager":{"iconName":"org.gnome.ArchiveManager","enabled":true},"Quod Libet":{"iconName":"io.github.quodlibet.QuodLibet","enabled":true},"Disks":{"iconName":"org.gnome.DiskUtility","enabled":true}}
          '';
        };

      "org/gnome/shell/extensions/gsconnect/device/E270A56204934FCDA149F0918D90345A/plugin/runcommand" =
        {
          command-list = "@a{sv} {}";
        };

      "org/gnome/shell/extensions/gsconnect/device/E270A56204934FCDA149F0918D90345A/plugin/share" =
        {
          receive-directory = "/home/dag/dwn";
        };

      "org/gnome/shell/extensions/gsconnect/preferences" = {
        window-maximized = true;
      };

      "org/gnome/shell/extensions/just-perfection" = {
        accessibility-menu = false;
        activities-button = true;
        aggregate-menu = true;
        animation = 4;
        background-menu = true;
        clock-menu = true;
        clock-menu-position = 0;
        clock-menu-position-offset = 0;
        dash = false;
        dash-icon-size = 0;
        dash-separator = false;
        double-super-to-appgrid = false;
        gesture = true;
        hot-corner = false;
        keyboard-layout = true;
        osd = true;
        panel = true;
        panel-arrow = true;
        panel-corner-size = 0;
        panel-in-overview = true;
        panel-notification-icon = true;
        panel-size = 0;
        power-icon = true;
        quick-settings = true;
        ripple-box = false;
        search = true;
        show-apps-button = false;
        startup-status = 0;
        switcher-popup-delay = false;
        theme = true;
        top-panel-position = 0;
        window-demands-attention-focus = false;
        window-picker-icon = true;
        window-preview-caption = true;
        window-preview-close-button = true;
        workspace = true;
        workspace-background-corner-size = 0;
        workspace-popup = true;
        workspace-switcher-should-show = false;
        workspace-switcher-size = 0;
        workspace-wrap-around = false;
        workspaces-in-app-grid = true;
        world-clock = false;
      };

      "org/gnome/shell/weather" = {
        automatic-location = true;
      };

      "org/gnome/shell/keybindings" = {
        switch-to-application-1 = [];
        switch-to-application-2 = [];
        switch-to-application-3 = [];
        switch-to-application-4 = [];
        switch-to-application-5 = [];
        switch-to-application-6 = [];
        switch-to-application-7 = [];
        toggle-message-tray = [ "<${modKey}>m" ];
      };

      "org/gnome/tweaks" = { show-extensions-notice = false; };

    };
  };
}
