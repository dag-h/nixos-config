{ pkgs, config, lib, ... }:
with lib;

let
  cfg = config.dag.music;
in {
  options.dag.music = {
    enable = mkOption {
      description = "Enable a set of music-related packages";
      type = types.bool;
      default = false;
    };
  };

  config = mkIf (cfg.enable) {
    home.packages = with pkgs; [
      flac
      cider
      nicotine-plus
      quodlibet-full
      easyeffects
      reaper
      bitwig-studio
    ];
  };
}
