{ pkgs, config, lib, ... }:
with lib;

let
  cfg = config.dag.sway;
in {

  options.dag.sway = {
    enable = mkEnableOption "Enable sway";
  };

  config = mkIf (cfg.enable) {
    wayland.windowManager.sway = {
      enable = true;
      systemd.enable = true;
      xwayland = true;
      config = rec {
        up = "h";
        down = "k";
        left = "j";
        right = "l";
        modifier = "Mod4";
        keybindings = let
          modifier = config.wayland.windowManager.sway.config.modifier;
        in lib.mkOptionDefault {
          "${modifier}+Return" = "exec ${pkgs.foot}/bin/foot";
          "${modifier}+Shift+q" = "kill";
          "${modifier}+d" = "exec ${pkgs.bemenu}/bin/bemenu-run";
        };
        startup = [
          { command = "firefox"; }
        ];
      };
    };
  };
  
}
