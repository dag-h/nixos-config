{ pkgs, config, lib, ... }:
with lib;

let
  cfg = config.dag.tmux;
in {
  options.dag.tmux = {
    enable = mkOption {
      description = "Enable and configure tmux";
      type = types.bool;
      default = false;
    };
  };

  config = mkIf (cfg.enable) {
    programs.tmux = {
      enable = true;
      clock24 = true;
      sensibleOnTop = true;
      extraConfig = ''
        bind r source-file ~/.config/tmux/tmux.conf
        unbind C-b
        set -g prefix C-Space
        bind C-Space send-prefix
        bind h split-window -h
        bind v split-window -v
        bind k kill-window

        set -g status-style 'bg=#dce0e8 fg=#4c4f69'
        set -g window-status-current-style 'bg=#4c4f69,fg=#dce0e8'
        set -g window-status-format " #I:#W "
        set -g window-status-current-format " #I:#W "

        set -g status-left '''
        set -g status-right '''

        bind -n M-j select-pane -L
        bind -n M-l select-pane -R
        bind -n M-h select-pane -U
        bind -n M-k select-pane -D
      '';
      plugins = with pkgs; [
        tmuxPlugins.sensible
      ];
    };
  };
}
