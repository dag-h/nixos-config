{ pkgs, config, lib, ... }:
with lib;

let
  cfg = config.dag.zellij;
in {
  options.dag.zellij = {
    enable = mkOption {
      description = "Enable and configure zellij";
      type = types.bool;
      default = false;
    };
  };

  config = mkIf (cfg.enable) {
    programs.zellij = {
      enable = true;
      enableZshIntegration = true;
      settings = {
        keybinds = {
          normal = {
          };
        };
      };
    };
  };
}
