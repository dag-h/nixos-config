{ pkgs, config, lib, ... }:
with lib;

let
  cfg = config.dag.bspwm;
in {
  options.dag.bspwm = {
    enable = mkEnableOption "Enable bspwm";
  };

  config = mkIf (cfg.enable) {
    home = {
      pointerCursor = {
        name = "Vanilla-DMZ";
        package = pkgs.vanilla-dmz;
        size = 20;
        x11.enable = true;
      };
      keyboard = {
        options = [ "ctrl:nocaps" ];
      };
    };

    xsession = {
      enable = true;
      scriptPath = ".hm-xsession";

      windowManager = {
        bspwm = {
          enable = true;
          monitors = {
            DP-0 = [ "term" "www" "emacs" "discord" "gaming" ];
            DVI-I-1 = [ "music" "misc" ];
          };
          settings = {
            active_border_color = "#2f1e2e";
            normal_border_color = "#2f1e2e";
            focused_border_color = "#2f1e2e";
            window_gap = 20;
            pointer_follows_focus = true;
            pointer_follows_monitor = true;

          };
          rules = {
            "Nextcloud" = {
              desktop = "misc";
              state = "floating";
            };
            "Bitwarden" = {
              desktop = "misc";
              state = "floating";
            };
            "Lutris" = {
              desktop = "gaming";
            };
            "riotclientux.exe" = {
              desktop = "gaming";
            };
            "leagueclient.exe" = {
              desktop = "gaming";
            };
          };
        };
        herbstluftwm = {
          enable = false;
          tags = [ "term" "www" "emacs" "disc" "game" "music" "misc" ];
          settings = {
            auto_detect_monitors = true;
            frame_bg_transparent = true;
            update_dragged_clients = 1;
            focus_follows_mouse = 1;
          };
          extraConfig = ''
            herbstclient detect_monitors
          '';
          keybinds = {
            Mod1-Shift-q = "quit";
            Mod1-Shift-r = "reload";
            Mod1-q = "close_and_remove";
            Mod1-Return = "spawn kitty";
            Mod1-d = "spawn rofi -show drun";
            Mod1-h = "focus left";
            Mod1-l = "focus right";
            Mod1-j = "focus down";
            Mod1-k = "focus up";
            Mod1-Shift-h = "shift left";
            Mod1-Shift-l = "shift right";
            Mod1-Shift-j = "shift down";
            Mod1-Shift-k = "shift up";
            Mod1-Tab = "cycle_all +1";
            Mod1-Shift-Tab = "cycle_all -1";
            Mod1-Control-h = "resize left 0.02";
            Mod1-Control-l = "resize right 0.02";
            Mod1-Control-j = "resize down 0.02";
            Mod1-Control-k = "resize up 0.02";
            Mod1-1 = "use term";
            Mod1-2 = "use www";
            Mod1-3 = "use emacs";
            Mod1-4 = "use disc";
            Mod1-5 = "use game";
            Mod1-6 = "use music";
            Mod1-7 = "use misc";
            Mod1-Shift-1 = "move term";
            Mod1-Shift-2 = "move www";
            Mod1-Shift-3 = "move emacs";
            Mod1-Shift-4 = "move disc";
            Mod1-Shift-5 = "move game";
            Mod1-Shift-6 = "move music";
            Mod1-Shift-7 = "move misc";
            Mod1-f = "fullscreen toggle";
          };
        };
      };
    };

    services = {
      unclutter.enable = true;

      random-background = {
        enable = true;
        enableXinerama = true;
        imageDirectory = "%h/media/pix/wallpapers";
      };

      gnome-keyring = {
        enable = true;
        components = [ "pkcs11" "secrets" "ssh" ];
      };

      # picom = {
      #   enable = true;
      #   inactiveOpacity = "0.8";
      #   experimentalBackends = true;
      #   blur = true;
      #   opacityRule = [
      #     "100:fullscreen"
      #   ];
      #   # inactiveDim = "0.1";
      #   fade = true;
      #   fadeDelta = 5;
      #   # don't fade in/out menus like the firefox menu
      #   fadeExclude = [ "window_type *= 'menu'" ];
      #   shadow = true;
      #   # remove shadow from firefox right click menu
      #   shadowExclude = [
      #     "class_g = 'firefox' && window_type *= 'utility'"
      #     "window_type *= 'menu'"
      #   ];
      #   # remove dim around firefox menus by excluding it from focus settings
      #   extraOptions = ''
      #     focus-exclude = [
      #         "class_g = 'firefox' && window_type *= 'utility'",
      #     ];
      #     blur-method = "kawase";
      #     blur-background-frame = false;
      #   '';
      #   vSync = true;
      # };

      sxhkd = {
        enable = true;
        keybindings = {
          "alt + Return" = "kitty";
          "alt + d" = "rofi -show drun";
          # quit/restart bspwm
          "alt + shift + {q,r}" = "bspc {quit,wm -r}";

          # close and kill
          "alt + {_,shift + }q" = "bspc node -{c,k}";

          # alternate between the tiled and monocle layout
          "alt + m" = "bspc desktop -l next";

          # send the newest marked node to the newest preselected node
          "alt + y" = "bspc node newest.marked.local -n newest.!automatic.local";

          # swap the current node and the biggest window
          "alt + g" = "bspc node -s biggest.window";

          #
          # state/flags
          #

          # set the window state
          "alt + {t,shift + t,s,f}" = "bspc node -t {tiled,pseudo_tiled,floating,fullscreen}";

          # set the node flags
          "alt + shift + {m,x,y,z}" = "bspc node -g {marked,locked,sticky,private}";

          #
          # focus/swap
          #

          # focus the node in the given direction
          "alt + {_,shift + }{h,j,k,l}" = "bspc node -{f,s} {west,south,north,east}";

          # focus the node for the given path jump
          #"alt + {p,b,comma,period}" = "bspc node -f @{parent,brother,first,second}";

          # focus the next/previous window in the current desktop
          "alt + {_,shift + }c" = "bspc node -f {next,prev}.local.!hidden.window";

          # focus the next/previous desktop in the current monitor
          "alt + bracket{left,right}" = "bspc desktop -f {prev,next}.local";

          # focus the older or newer node in the focus history
          #"alt + {o,i}" = "bspc wm -h off; \ bspc node {older,newer} -f; \ bspc wm -h on";

          #
          # preselect
          #

          # preselect the direction
          "alt + ctrl + {h,j,k,l}" = "bspc node -p {west,south,north,east}";

          # preselect the ratio
          "alt + ctrl + {1-9}" = "bspc node -o 0.{1-9}";

          # cancel the preselection for the focused node
          "alt + ctrl + space" = "bspc node -p cancel";

          # cancel the preselection for the focused desktop
          "alt + ctrl + shift + space" = "bspc query -N -d | xargs -I id -n 1 bspc node id -p cancel";

          # focus or send to the given desktop
          #"alt + {_,shift + }{1-9,0}" = "bspc {desktop -f,node -d} '^{1-9,10}'";

          "alt + {_,shift +}1" = "bspc {desktop -f,node -d} term";
          "alt + {_,shift +}2" = "bspc {desktop -f,node -d} www";
          "alt + {_,shift +}3" = "bspc {desktop -f,node -d} emacs";
          "alt + {_,shift +}4" = "bspc {desktop -f,node -d} discord";
          "alt + {_,shift +}5" = "bspc {desktop -f,node -d} gaming";
          "alt + {_,shift +}6" = "bspc {desktop -f,node -d} music";
          "alt + {_,shift +}7" = "bspc {desktop -f,node -d} misc";

          "alt + space" = ''
            focused=`bspc query -D -d focused`; bspc desktop -m prev; bspc monitor -f prev; bspc desktop -f $focused
          '';

          #
          # move/resize
          #

          # expand a window by moving one of its side outward
          "super + alt + {h,j,k,l}" = "bspc node -z {left -20 0,bottom 0 20,top 0 -20,right 20 0}";

          # contract a window by moving one of its side inward
          "super + alt + shift + {h,j,k,l}" = "bspc node -z {right -20 0,top 0 20,bottom 0 -20,left 20 0}";

          # move a floating window
          "alt + {Left,Down,Up,Right}" = "bspc node -v {-20 0,0 20,0 -20,20 0}";

          "alt + {o,p}" = "change_gaps.sh {dec, inc}";
          "alt + {u,i}" = "bspc node -f {prev,next}.local";

          "XF86AudioMute" = "pactl set-sink-mute @DEFAULT_SINK@ toggle";
          "XF86AudioLowerVolume" = "pactl set-sink-volume @DEFAULT_SINK@ -10%";
          "XF86AudioRaiseVolume" = "pactl set-sink-volume @DEFAULT_SINK@ +10%";
        };
      };
    };

    programs = {
      rofi =
        let
          inherit (config.lib.formats.rasi) mkLiteral;
        in {
          enable = true;
          font = "Roboto 10";
          extraConfig = {
            "modi" = mkLiteral "[window,run,ssh,drun]";
            "width" = 1920;
	          "lines" = 1;
            "yoffset" = 0;
	          "columns" = 20;
	          "bw" = 10;
	          "padding" = 20;
	          "eh" = 1;
	          "separator-style" = "solid";
	          "hide-scrollbar" = true;
	          "fullscreen" = true;
            "line-padding" = 10;
          };
      };
      qutebrowser = {
        enable = true;
        extraConfig = ''
          c.content.blocking.adblock.lists = ["https://austinhuang.me/0131-block-list/list.txt",
                                    "https://raw.githubusercontent.com/brave/adblock-lists/master/brave-unbreak.txt",
                                    "https://easylist.to/easylist/fanboy-annoyance.txt",
                                    "https://easylist.to/easylist/fanboy-social.txt",
                                    "https://easylist.to/easylist/easyprivacy.txt",
                                    "https://easylist.to/easylist/easylist.txt",
                                    "https://badmojr.github.io/1Hosts/mini/adblock.txt",
                                    "https://badmojr.github.io/1Hosts/Lite/adblock.txt",
                                    "https://badmojr.github.io/1Hosts/Pro/adblock.txt",
                                    "https://pgl.yoyo.org/adservers/serverlist.php?hostformat=adblockplus&showintro=2&mimetype=plaintext",
                                    "https://raw.githubusercontent.com/brave/adblock-lists/master/brave-unbreak.txt",
                                    "https://raw.githubusercontent.com/brave/adblock-lists/master/coin-miners.txt",
                                    "https://raw.githubusercontent.com/brave/adblock-lists/master/brave-lists/brave-social.txt",
                                    "https://raw.githubusercontent.com/brave/adblock-lists/master/brave-lists/brave-specific.txt",
                                    "https://raw.githubusercontent.com/brave/adblock-lists/master/brave-lists/brave-unbreak.txt",
                                    ]
        '';
      };
    };

    home.packages = with pkgs; [
      gcr
      pavucontrol
      python310Packages.adblock
      pulseaudio
      playerctl
    ];

  };
}
