{ pkgs, config, lib, ... }:
with lib;

let
  cfg = config.dag.gaming;

in {
  options.dag.gaming = {
    enable = mkOption {
      description = "Enable a set of gaming-related packages";
      type = types.bool;
      default = false;
    };
  };

  config = mkIf (cfg.enable) {

    home.file = {
      autoexec = {
        source = ../../../files/autoexec.cfg;
        target = ".local/Steam/steamapps/common/Team Fortress 2/tf/cfg/autoexec.cfg";
      };
      engineer = {
        source = ../../../files/engineer.cfg;
        target = ".local/Steam/steamapps/common/Team Fortress 2/tf/cfg/engineer.cfg";
      };
    };

    home.packages = with pkgs; [
      vulkan-tools
      vulkan-loader
      lutris
      wine-staging
      winetricks
      minecraft
      openjdk
    ];
  };
}
