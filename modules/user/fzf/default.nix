{ pkgs, config, lib, ... }:
with lib;

let
  cfg = config.dag.fzf;
in {
  options.dag.fzf = {
    enable = mkOption {
      description = "Enable and configure fzf";
      type = types.bool;
      default = false;
    };
  };

  config = mkIf (cfg.enable) {
    home.packages = with pkgs; [
      fd
    ];

    programs.fzf = {
      enable = true;
      enableZshIntegration = true;
      defaultCommand = "${pkgs.fd.pname} --type f";
    };
  };
}
