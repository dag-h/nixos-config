{ pkgs, config, lib, ... }:
with lib;

let
  cfg = config.dag.base;
in {
  options.dag.base = {
    enable = mkEnableOption "Enable a set of base packages, used on every system";
  };

  config = mkIf (cfg.enable) {
    home.packages = with pkgs; [
      # tools
      ripgrep
      fd
      bat
      eza
      unzip

      # latex
      # texlive.combined.scheme-full
      python39Packages.pygments

      # misc
      rmapi
      bitwarden
      nextcloud-client
      obs-studio
      wl-clipboard
      helvum
      elektroid
      zathura
      fortune
      obsidian
      cartridges
      webcord
      instawow
      (prismlauncher.override { jdks = [ jdk8 jdk17 jdk19 ]; })
      typst
      typst-lsp
      unityhub

      mullvad-vpn
      harfbuzz
      (aspellWithDicts (dicts: with dicts; [
        en sv
      ]))
      nix-index

      # development
      (tree-sitter.withPlugins (p: [ p.tree-sitter-cpp ]))
      nix-ld
      openjdk
      parinfer-rust
      neovide
      vscode
    ];

    home.sessionVariables = {
      BAT_THEME = "Solarized (light)";
      QT_QPA_PLATFORM = "wayland";
      NEOVIDE_FRAME = "none";
    };

    programs = {
      direnv = {
        enable = true;
        enableZshIntegration = true;
        nix-direnv.enable = true;
      };

      gpg = {
        enable = true;
      };

      ssh = {
        enable = true;
        extraConfig = ''
        Host pi
             HostName cichli
             User pi
             Port 69

        Host remarkable
             HostName 192.168.1.200
             User root
        '';     
      };
    };

    services = {
      gpg-agent.enable = true;

      nextcloud-client = {
        enable = true;
        startInBackground = true;
      };
    };
  };
}
