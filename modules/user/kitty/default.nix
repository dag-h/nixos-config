{ pkgs, config, lib, ... }:
with lib;

let
  cfg = config.dag.kitty;
  theme = "catppuccin-latte.conf";
in {
  options.dag.kitty = {
    enable = mkOption {
      description = "Enable kitty with configurations";
      type = types.bool;
      default = false;
    };
  };

  config = mkIf (cfg.enable) {

    home.file.catppuccin = {
      source = ../../../files + "/${theme}";
      target = ".config/kitty/themes/${theme}";
    };
    programs.kitty = {
      enable = true;
      package = pkgs.kitty;
      font = {
        name = "JetBrains Mono";
        package = pkgs.jetbrains-mono;
        size = 12;
      };
      keybindings = {
        "ctrl+shift+minus" = "change_font_size all -2.0";
        "ctrl+shift+plus" = "change_font_size all +2.0";
        "ctrl+j" = "scroll_line_down";
        "ctrl+k" = "scroll_line_up";
        "ctrl+shift+d" = "scroll_page_down";
        "ctrl+shift+u" = "scroll_page_up";
      };
      settings = {
        confirm_os_window_close = 0;
        window_margin_width = 18;
        remember_window_size = "no";
        initial_window_width = 700;
        initial_window_height = 500;
        enable_audio_bell = "no";
        hide_window_decorations = "yes";
        include = "~/.config/kitty/themes/${theme}";
      };
    };
  };
}
