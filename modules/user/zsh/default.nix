{ pkgs, config, lib, ... }:
with lib;

let
  cfg = config.dag.zsh;
in {
  options.dag.zsh = {
    enable = mkOption {
      description = "Enable zsh with configurations";
      type = types.bool;
      default = false;
    };
  };

  config = mkIf (cfg.enable) {
    programs.zsh = {
      enable = true;
      dotDir = ".config/zsh";
      autosuggestion.enable = true;
      enableCompletion = true;
      syntaxHighlighting.enable = true;
      initExtra = ''
        zstyle ':completion:*' special-dirs true
        alias nix-search="nix search nixpkgs"
        alias ls="eza"
        alias ec="emacsclient"
        alias thr="WINEPREFIX=/home/dag/wine/wine32 wine /home/dag/wine/wine32/drive_c/Program\ Files/Yamaha/YAMAHA\ THR\ Editor/THR\ Editor.exe"
        alias codium="codium --ozone-platform=wayland --enable-features=WaylandWindowDecorations"
        alias nv="neovide"

        autoload -Uz vcs_info
        precmd_vcs_info() { vcs_info }
        precmd_functions+=( precmd_vcs_info )
        setopt prompt_subst
        RPROMPT=\$vcs_info_msg_0_
        zstyle ':vcs_info:git:*' formats '%F{240}%b%f'
        zstyle ':vcs_info:*' enable git
        PROMPT="%B%F{blue}%~%f%b %m > "
        bindkey "^?" backward-delete-char
      '';
      defaultKeymap = "viins";
    };
  };
}
