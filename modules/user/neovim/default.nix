{ pkgs, config, lib, ... }:

with lib;

let
  cfg = config.dag.neovim;
in {
  options.dag.neovim = {
    enable = mkOption {
      description = "Enable neovim with configurations";
      type = types.bool;
      default = false;
    };
  };

  config = mkIf (cfg.enable) {
    home.sessionVariables = {
      EDITOR = "nvim";
    };

    programs.neovim = let
      plugin-folder = ../../../files/neovim/plugin;
    in {
      enable = true;
      package = pkgs.neovim-unwrapped;
      vimAlias = true;
      viAlias = true;
      extraLuaConfig = builtins.readFile ../../../files/neovim/init.lua;

      plugins = with pkgs.vimPlugins; [
        typst-vim
        direnv-vim
        {
          plugin = alpha-nvim;
          type = "lua";
          config = builtins.readFile (plugin-folder + /alpha-nvim.lua);
        }
        {
          plugin = project-nvim;
          type = "lua";
          config = builtins.readFile (plugin-folder + /project-nvim.lua);
        }
        {
          plugin = leap-nvim;
          type = "lua";
          config = builtins.readFile (plugin-folder + /leap-nvim.lua);
        }
        vim-nix
        cmp-nvim-lsp
        cmp-buffer
        cmp-path
        cmp-cmdline
        luasnip
        {
          plugin = nvim-cmp;
          type = "lua";
          config = builtins.readFile (plugin-folder + /nvim-cmp.lua);
        }
        {
          plugin = nvim-lspconfig;
          type = "lua";
          config = builtins.readFile (plugin-folder + /nvim-lspconfig.lua);
        }
        {
          plugin = telescope-nvim;
          type = "lua";
          config = builtins.readFile (plugin-folder + /telescope.lua);
        }
        {
          plugin = nvim-treesitter.withAllGrammars;
          type = "lua";
          config = builtins.readFile (plugin-folder + /nvim-treesitter.lua);
        }
        rustaceanvim
        {
          plugin = catppuccin-nvim;
          type = "lua";
          config = builtins.readFile (plugin-folder + /catppuccin-nvim.lua);
        }
        {
          plugin = neogit;
          type = "lua";
          config = builtins.readFile (plugin-folder + /neogit.lua);
        }
        {
          plugin = neorg;
          type = "lua";
          config = builtins.readFile (plugin-folder + /neorg.lua);
        }
        {
          plugin = comment-nvim;
          type = "lua";
          config = builtins.readFile (plugin-folder + /comment-nvim.lua);
        }
        {
          plugin = toggleterm-nvim;
          type = "lua";
          config = builtins.readFile (plugin-folder + /toggleterm.lua);
        }
        {
          plugin = vimtex;
          type = "lua";
          config = builtins.readFile (plugin-folder + /vimtex.lua);
        }
      ];
    };
  };
}
