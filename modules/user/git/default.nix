{ pkgs, config, lib, ... }:
with lib;

let
  cfg = config.dag.git;
in {
  options.dag.git = {
    enable = mkOption {
      description = "Enable and configure git";
      type = types.bool;
      default = false;
    };
    user = mkOption {
      description = "The user name to use for git";
      type = types.str;
      default = null;
    };
    email = mkOption {
      description = "The email address to use for git";
      type = types.str;
      default = null;
    };
  };

  config = mkIf (cfg.enable) {
    programs.git = {
      enable = true;
      lfs.enable = true;
      userEmail = cfg.email;
      userName = cfg.user;
      extraConfig = {
        pull.rebase = true;
      };
    };
  };
}
