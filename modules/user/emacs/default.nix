{ pkgs, config, lib, ... }:
with lib;

let
  cfg = config.dag.emacs;
in {
  options.dag.emacs = {
    enable = mkEnableOption "Enable emacs package and configuration";
  };

  config = mkIf (cfg.enable) {
    home.file = {
      dag-theme = {
        source = ../../../files/dag-theme.el;
        target = ".config/emacs/dag-theme.el";
      };
      snippets = {
        source = ../../../files/snippets;
        target = ".config/emacs/snippets";
        recursive = true;
      };
      config = {
        source = ../../../files/config.org;
        target = ".config/emacs/config.org";
        onChange = "rm -f .config/emacs/config.el";
      };
    };
    programs = {
      emacs = {
        enable = false;
        package = (pkgs.emacsWithPackagesFromUsePackage {
          config = ../../../files/config.org;
          package = pkgs.emacs-pgtk;
          alwaysEnsure = false;
          extraEmacsPackages = (epkgs: (with epkgs; [
            vterm
            use-package
            tree-sitter
            treesit-grammars.with-all-grammars
            parinfer-rust-mode
          ]));
        });
      };
    };
  };
}
