{ pkgs, config, lib, ... }:

{
  imports = [
    ./base
    ./fzf
    ./kitty
    ./neovim
    ./zsh
    ./xdg
    ./music
    ./gnome-extra
    ./gaming
    ./git
    ./social
    ./bspwm
    ./rpi
    ./sway
    ./alacritty
    ./zellij
    ./tmux
    ./emacs
  ];
}
