{ pkgs, config, lib, ... }:
with lib;

let
  cfg = config.dag.xdg;
  homeDir = config.home.homeDirectory;
in {
  options.dag.xdg = {
    enable = mkOption {
      description = "Enable xdg configuration";
      type = types.bool;
      default = false;
    };
  };

  config = mkIf (cfg.enable) {
    xdg = {
      enable = true;
      configFile."mimeapps.list".force = true;
      cacheHome = /. + "${homeDir}" + /.cache;
      configHome = /. + "${homeDir}" + /.config;
      dataHome = /. + "${homeDir}" + /.local/share;
      userDirs = {
        enable = true;
        createDirectories = false;
        desktop = "${homeDir}";
        documents = "${homeDir}/cloud/docs";
        download = "${homeDir}/dwn";
        music = "${homeDir}/cloud/music"; 
        pictures = "${homeDir}/media/pix";
        publicShare = homeDir;
        templates = homeDir;
        videos = "${homeDir}/media/vids";
      };
      mimeApps = {
        enable = true;
        defaultApplications = {
          "video/mp4" = [ "org.gnome.Totem.desktop" ];
          "application/pdf" = [ "org.gnome.Evince.desktop" ];
          "x-scheme-handler/https" = [ "firefox.desktop" ];
          "x-scheme-handler/http" = [ "firefox.desktop" ];
          "x-scheme-handler/msteams" = [ "teams.desktop" ];
        };
      };
    };
  };
}
