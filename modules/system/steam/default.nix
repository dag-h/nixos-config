{ pkgs, config, lib, ... }:
with lib;

let
  cfg = config.dag.steam;
in {
  options.dag.steam = {
    enable = mkOption {
      description = "Enable Steam for the system";
      type = types.bool;
      default = false;
    };
  };

  config = mkIf cfg.enable {
    programs.steam = {
      enable = true;     
      remotePlay.openFirewall = true;
      dedicatedServer.openFirewall = true;
    };
    hardware.steam-hardware.enable = true;
  };

}
