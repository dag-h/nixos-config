{ pkgs, config, lib, ... }:
with lib;

let
  cfg = config.dag.printing;
in {
  options.dag.printing = {
    enable = mkOption {
      description = "Enable printing support";
      type = types.bool;
      default = false;
    };
  };

  config = mkIf (cfg.enable) {
    services.printing = {
      enable = true;
      drivers = [ pkgs.hplip ];
    };
  };

}
