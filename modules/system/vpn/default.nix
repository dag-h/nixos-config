{ pkgs, config, lib, ... }:
with lib;

let
  cfg = config.dag.vpn;
in {
  options.dag.vpn = {
    enable = mkOption {
      description = "Enable VPN service using mullvad-vpn";
      type = types.bool;
      default = false;
    };
  };

  config = mkIf (cfg.enable) {
    services.mullvad-vpn.enable = true;

    environment.systemPackages = with pkgs; [
      mullvad-vpn
    ];
  };
}
