{ pkgs, config, lib, ... }:
with lib;

let
  cfg = config.dag.adb;
  userList = (map (key: getAttr key config.users.users) (attrNames config.users.users));
  normalUsers = filter (user: user.isNormalUser) userList;
  normalUserNames = map (user: user.name) normalUsers;
in {
  options.dag.adb = {
    enable = mkOption {
      description = "Enable Android Debug Bridge for the system";
      type = types.bool;
      default = false;
    };
  };

  config = mkIf (cfg.enable) {
    programs.adb.enable = true;

    # add all normal users to the adbusers group
    users.groups.adbusers.members = normalUserNames;
  };

}
