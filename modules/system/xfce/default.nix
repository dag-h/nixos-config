{ pkgs, config, lib, ... }:
with lib;

let
  cfg = config.dag.xfce;
in {
  options.dag.xfce = {
    enable = mkOption {
      description = "Enable the Xfce desktop environment and associated packages";
      type = types.bool;
      default = false;
    };
  };

  config = mkIf (cfg.enable) {
    services.xserver = {
      enable = true;
      displayManager.lightdm.enable = true;
      desktopManager.xfce.enable = true;
    };

    # For KDE Connect
    networking.firewall.extraCommands =
      ''
        iptables -A nixos-fw -p tcp --source 192.168.1.0/24 --dport 1714:1764 -j nixos-fw-accept
        iptables -A nixos-fw -p udp --source 192.168.1.0/24 --dport 1714:1764 -j nixos-fw-accept
      '';
  };

}
