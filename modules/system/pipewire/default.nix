{ pkgs, config, lib, ... }:
with lib;

let
  cfg = config.dag.pipewire;
  userList = (map (key: getAttr key config.users.users) (attrNames config.users.users));
  normalUsers = filter (user: user.isNormalUser) userList;
  normalUserNames = map (user: user.name) normalUsers;
in {
  options.dag.pipewire = {
    enable = mkOption {
      description = "Enable pipewire for the system";
      type = types.bool;
      default = false;
    };
  };

  config = mkIf (cfg.enable) {
    users.groups.pipewire.members = normalUserNames;
    users.groups.audio.members = normalUserNames;
    hardware.pulseaudio.enable = false;

    security.rtkit.enable = true;

    security.pam.loginLimits = [
      { domain = "@pipewire"; item = "rtprio"; type = "-"; value = "95"; }
      { domain = "@pipewire"; item = "nice"; type = "-"; value = "-19"; }
      { domain = "@pipewire"; item = "memlock"; type = "-"; value = "4194304"; }
    ];    

    services.pipewire = {
      enable = true;
      alsa.enable = true;
      alsa.support32Bit = true;
      pulse.enable = true;
      jack.enable = true;
    };
  };

}
