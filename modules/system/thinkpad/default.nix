{ pkgs, config, lib, ... }:
with lib;

let
  cfg = config.dag.thinkpad;
in {
  options.dag.thinkpad = {
    enable = mkOption {
      description = "Enable configurations specific to my Lenovo Thinkpad X1 Carbon laptop";
      type = types.bool;
      default = false;
    };
  };

  config = mkIf (cfg.enable) {
    programs.wireshark.enable = true;

    # TODO: move to expo module or something
    networking.firewall.allowedTCPPorts = [
      19000
    ];

    security.pki.certificateFiles = [
      ../../../files/liu-cert.crt
    ];

    services.udev = {
      extraRules = ''
        SUBSYSTEM=="usb", ATTRS{idVendor}=="1935", MODE="0666"
        SUBSYSTEM=="usb_device", ATTRS{idVendor}=="1935", MODE="0666"
      '';
      extraHwdb = ''
        usb:v1935*
         ID_VENDOR_FROM_DATABASE=Elektron Music Machines

        usb:v1935p0003*
         ID_MODEL_FROM_DATABASE=Analog Four MIDI
        
        usb:v1935p0004*
         ID_MODEL_FROM_DATABASE=Analog Four Overbridge
        
        usb:v1935p000C*
         ID_MODEL_FROM_DATABASE=Digitakt Overbridge
        
        usb:v1935p000D*
         ID_MODEL_FROM_DATABASE=Digitakt MIDI
        
        usb:v1935p102C*
         ID_MODEL_FROM_DATABASE=Digitakt Audio/MIDI
        
        usb:v1935p001E*
         ID_MODEL_FROM_DATABASE=Syntakt Overbridge
      '';
    };

    environment.etc = {
	    "wireplumber/bluetooth.lua.d/51-bluez-config.lua".text = ''
		    bluez_monitor.properties = {
			      ["bluez5.enable-sbc-xq"] = true,
			      ["bluez5.enable-msbc"] = true,
			      ["bluez5.enable-hw-volume"] = true,
			      ["bluez5.headset-roles"] = "[ hsp_hs hsp_ag hfp_hf hfp_ag ]"
		    }
      '';
    };

    environment.systemPackages = with pkgs; [
      egl-wayland
    ];

    hardware = {
      # needed for iwlwifi to find firmware
      enableRedistributableFirmware = true;
      cpu.intel.updateMicrocode = true;
      opengl = {
        driSupport32Bit = true;
      };

      trackpoint = {
        enable = true;
        device = "tpps/2 elan trackpoint";
        emulateWheel = true;
        speed = 50;
      };
    };

    powerManagement.cpuFreqGovernor = lib.mkDefault "powersave";
  };

}
