{ pkgs, config, lib, ... }:
with lib;

let
  cfg = config.dag.rpi;
in {
  options.dag.rpi = {
    enable = mkOption {
      description = "Enable Raspberry Pi 4 system configurations";
      type = types.bool;
      default = false;
    };
    autologinUser = mkOption {
      description = "User to automatically log in on system startup";
      type = types.str;
      default = null;
    };
  };

  config = mkIf (cfg.enable) {
    boot.tmp.useTmpfs = true;
    hardware = if builtins.hasAttr "raspberry-pi" config.hardware then {
      enableRedistributableFirmware = true;
      raspberry-pi."4".fkms-3d.enable = true;
    } else { };

    users.users.dag.openssh.authorizedKeys.keys = [ "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIAp7l2vR0XfL5CzxqN1ovXw9d07uojwe2jD8o6kNJcMh dagharju@protonmail.com" ];

    services.openssh.settings.PasswordAuthentication = true;
    
    services.getty.autologinUser = cfg.autologinUser;

    services.minecraft-server = {
      enable = false;
      eula = true;
      openFirewall = true;
      declarative = true;
      serverProperties = {
        server-port = 25565;
        difficulty = "hard";
        gamemode = "survival";
        max-players = 10;
        motd = "NixOS Minecraft Server";
        white-list = true;
      };
      whitelist = {
        __dag__ = "d3fb1e41-5689-4ed7-a193-f8992f59c670";
        crackaddey = "e569b07f-ee9e-4407-8a5d-49f394f1d161";
        menjaghetersimon = "dd70eedb-c031-4afa-99e4-5b0489b1b2df";
        tobbe_trollkarl_ = "c8419431-ac03-4e20-ad85-562bbe458be9";
        simonbajs = "20f23915-8ccc-4d5d-a5ff-a56c0ea46f45";
      };
      jvmOpts = "-Xms4G -Xmx4G -XX:+UseG1GC -XX:+ParallelRefProcEnabled -XX:MaxGCPauseMillis=200 -XX:+UnlockExperimentalVMOptions -XX:+DisableExplicitGC -XX:+AlwaysPreTouch -XX:G1NewSizePercent=30 -XX:G1MaxNewSizePercent=40 -XX:G1HeapRegionSize=8M -XX:G1ReservePercent=20 -XX:G1HeapWastePercent=5 -XX:G1MixedGCCountTarget=4 -XX:InitiatingHeapOccupancyPercent=15 -XX:G1MixedGCLiveThresholdPercent=90 -XX:G1RSetUpdatingPauseTimePercent=5 -XX:SurvivorRatio=32 -XX:+PerfDisableSharedMem -XX:MaxTenuringThreshold=1 -Dusing.aikars.flags=https://mcflags.emc.gs -Daikars.new.flags=true";
    };

    users.mutableUsers = true;
  };
}
