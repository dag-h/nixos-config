{ pkgs, config, lib, ... }:
with lib;

let
  cfg = config.dag.virt;
  userList = (map (key: getAttr key config.users.users) (attrNames config.users.users));
  normalUsers = filter (user: user.isNormalUser) userList;
  normalUserNames = map (user: user.name) normalUsers;
in {
  options.dag.virt = {
    enable = mkOption {
      description = "Enable virtualization support for the system";
      type = types.bool;
      default = false;
    };
  };

  config = mkIf (cfg.enable) {
    virtualisation.libvirtd.enable = true;
    environment.systemPackages = with pkgs; [
      gnome.gnome-boxes
    ];
  };

}
