{ pkgs, config, lib, ... }:
with lib;

let
  cfg = config.dag.trackball;
in {
  options.dag.trackball = {
    enable = mkEnableOption "Enable trackball configuration";
  };

  config = mkIf (cfg.enable) {
    services.xserver.inputClassSections = [
      ''
        Identifier      "trackball"
        MatchDevicePath "/dev/input/event*"
        MatchProduct    "Expert Wireless TB Mouse"
        MatchVendor     "Kensington"                
        Option          "ScrollMethod"              "button"
        Option          "ScrollButtonLock"          "false"
        Option          "ScrollButton"              "3"
        Option          "ButtonMapping"             "1 2 8 4 5 6 7 3 9"
      ''
    ];
  };

}
