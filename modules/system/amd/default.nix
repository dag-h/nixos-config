{ pkgs, config, lib, ... }:
with lib;

let
  cfg = config.dag.amd;
in {
  options.dag.amd = {
    enable = mkOption {
      description = "Enable configurations for AMD hardware";
      type = types.bool;
      default = false;
    };
  };

  config = mkIf (cfg.enable) {
    hardware = {
      enableRedistributableFirmware = true;
      cpu.amd.updateMicrocode = true;
    };
  };

}
