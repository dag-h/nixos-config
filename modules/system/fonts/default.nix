{ pkgs, config, lib, ... }:
with lib;

let
  cfg = config.dag.fonts;
in {
  options.dag.fonts = {
    enable = mkOption {
      description = "Enables some useful fonts for the system";
      type = types.bool;
      default = false;
    };
  };

  config = mkIf (cfg.enable) {
    fonts.packages = with pkgs; [
      jetbrains-mono
      roboto
      roboto-mono
      roboto-slab
      # emacs-all-the-icons-fonts
      fira
      fira-code
      etBook
      victor-mono
      overpass
      nerdfonts
    ];
  };

}
