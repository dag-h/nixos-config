{ pkgs, config, lib, ... }:
with lib;

let
  cfg = config.dag.wireshark;
  userList = (map (key: getAttr key config.users.users) (attrNames config.users.users));
  normalUsers = filter (user: user.isNormalUser) userList;
  normalUserNames = map (user: user.name) normalUsers;
in {
  options.dag.wireshark = {
    enable = mkOption {
      description = "Enable wireshark for the system";
      type = types.bool;
      default = false;
    };
  };

  config = mkIf (cfg.enable) {
    programs.wireshark.enable = true;
    users.groups.wireshark.members = normalUserNames;
  };

}
