{ pkgs, config, lib, ... }:
with lib;

let
  cfg = config.dag.gnome;
in {
  options.dag.gnome = {
    enable = mkOption {
      description = "Enable the Gnome desktop environment and associated packages";
      type = types.bool;
      default = false;
    };
    wayland = mkOption {
      description = "Use Wayland for Gnome instead of X11.";
      type = types.bool;
      default = false;
    };
  };

  config = mkIf (cfg.enable) {
    services = {
      xserver = {
        enable = true;

        displayManager.gdm = {
          enable = true;
          wayland = cfg.wayland;
        };
        
        desktopManager.gnome.enable = true;
      };
    };

    systemd = {
      services = {
        # causes rebuilds to fail
        # see https://github.com/NixOS/nixpkgs/issues/180175
        NetworkManager-wait-online.enable = false;
      };
    };

    programs.xwayland.enable = cfg.wayland;
    programs.droidcam.enable = true;

    networking.firewall = {
      enable = true;
      allowedTCPPortRanges = [ 
        { from = 1714; to = 1764; } # KDE Connect
      ];  
      allowedUDPPortRanges = [ 
        { from = 1714; to = 1764; } # KDE Connect
      ];  
    };
  };
}
