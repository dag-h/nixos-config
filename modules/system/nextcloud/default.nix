{ pkgs, config, lib, ... }:
with lib;

let
  cfg = config.dag.nextcloud;
in {
  options.dag.nextcloud = {
    enable = mkOption {
      description = "Enable nextcloud server.";
      type = types.bool;
      default = false;
    };
  };

  config = mkIf (cfg.enable) {
    networking.firewall.allowedTCPPorts = [ 443 80 ];
    security.acme = {
      acceptTerms = true;
      defaults.email = "dagharju@proton.me";
    };
    systemd.services."nextcloud-setup" = {
      requires = [ "postgresql.service" ];
      after = [ "postgresql.service" ];
    };
    services = {
      nextcloud = {
        enable = true;
        enableImagemagick = true;
        hostName = "cloud.harju.xyz";
        package = pkgs.nextcloud27;
        caching.apcu = true;
        https = true;
        maxUploadSize = "8192M";
        config = {
          defaultPhoneRegion = "SE";
          overwriteProtocol = "https";
          dbtype = "pgsql";
          dbuser = "nextcloud";
          dbname = "nextcloud";
          dbhost = "/run/postgresql";
          dbpassFile = "/var/nextcloud-db-pass";
          adminpassFile = "/var/nextcloud-admin-pass";
          adminuser = "admin";
        };
        extraApps = {
          music = pkgs.fetchNextcloudApp rec {
            url = "https://github.com/owncloud/music/releases/download/v1.8.4/music_1.8.4_for_nextcloud.tar.gz";
            sha256 = "sha256-cJvXlUojVcWR22+KxNiI7pA0ngg6hyBVfo6DaVmRdTM=";
          };
          calendar = pkgs.fetchNextcloudApp rec {
            url = "https://github.com/nextcloud-releases/calendar/releases/download/v4.4.4/calendar-v4.4.4.tar.gz";
            sha256 = "sha256-+1jD05SY7Oz5GdICiQYXgPrzZLHDQgMaLj03OzvQPFI=";
          };
        };
        extraAppsEnable = true;
      };
      nginx = {
        enable = true;
        enableReload = true;
        recommendedGzipSettings = true;
        recommendedOptimisation = true;
        recommendedProxySettings = true;
        recommendedTlsSettings = true;
        sslCiphers = "AES256+EECDH:AES256+EDH:!aNULL";
        virtualHosts = {
          "cloud.harju.xyz" = {
            forceSSL = true;
            enableACME = true;
          };
          # "blog.harju.xyz" = {
          #   root = "/var/www/blog";
          #   forceSSL = true;
          #   enableACME = true;
          # };
        };
      };
      postgresql = {
        enable = true;
        ensureDatabases = [ "nextcloud" ];
        ensureUsers = [
          {
            name = "nextcloud";
            ensurePermissions."DATABASE nextcloud" = "ALL PRIVILEGES";
          }
        ];
      };
    };
  };

}
