{ pkgs, config, lib, ... }:
with lib;

let
  cfg = config.dag.boot;
in {
  options.dag.boot = {
    grub = mkOption {
      description = "Use grub instead of systemd-boot";
      type = types.bool;
      default = false;
    };

    grub-device = mkOption {
      description = "Device used for grub";
      type = types.str;
      default = "";
    };

    systemd-boot = mkOption {
      description = "Enable systemd-boot.";
      type = types.bool;
      default = false;
    };

    rpi = mkOption {
      description = "Enable raspberry pi boot configuration";
      type = types.bool;
      default = false;
    };
  };

  config = {
    boot = {
      loader = {
        grub = mkIf (cfg.grub) {
          enable = true;
          device = cfg.grub-device;
          useOSProber = true;
          efiSupport = true;
        };
        systemd-boot = mkIf (cfg.systemd-boot) {
          enable = !cfg.grub;
          memtest86.enable = true;
        };
        efi.canTouchEfiVariables = (cfg.systemd-boot || cfg.grub);
      };
    };
  };

}
