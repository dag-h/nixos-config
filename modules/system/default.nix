{ pkgs, config, lib, ... }:

{
  imports = [
    ./amd
    ./v4l2
    ./adb
    ./wireshark
    ./fonts
    ./pipewire
    ./bluetooth
    ./thinkpad
    ./gnome
    ./filesystem
    ./printing
    ./steam
    ./virt
    ./vpn
    ./boot
    ./trackball
    ./rpi
    ./wm
    ./musnix
    ./nextcloud
    ./xfce
  ];
}
