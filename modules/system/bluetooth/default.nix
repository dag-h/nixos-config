{ pkgs, config, lib, ... }:
with lib;

let
  cfg = config.dag.bluetooth;
in {
  options.dag.bluetooth = {
    enable = mkOption {
      description = "Enable bluetooth support for the system";
      type = types.bool;
      default = false;
    };
    asus-dongle = mkOption {
      description = "Add firmware for asus bluetooth dongle";
      type = types.bool;
      default = false;
    };
  };

  config = mkIf (cfg.enable) {

    environment.systemPackages = with pkgs; [
      (mkIf (cfg.asus-dongle) rtl8761b-firmware)
    ];
    
    hardware.bluetooth = {
      enable = true;
      settings = {
        General = {
          Enable = "Source,Sink,Media,Socket";
          # required to be able to connect AirPods Pro 2 for some reason
          ControllerMode = "bredr";
        };
        
      };
    };
  };
}
