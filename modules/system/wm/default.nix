{ pkgs, config, lib, ... }:
with lib;

let
  cfg = config.dag.wm;
in {
  options.dag.wm = {
    enable = mkEnableOption "Enable support for using a window manager";
  };

  config = mkIf (cfg.enable) {
    console.useXkbConfig = true;
    services.xserver = {
      enable = true;
    };
    programs.seahorse.enable = true;
    programs.dconf.enable = true;
    security.polkit.enable = true;
    services.greetd = {
      enable = true;
      settings = {
        default_session = {
          command = "${pkgs.greetd.greetd}/bin/agreety --cmd sway";
        };
      };
    };
  };
}
