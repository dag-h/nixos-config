{ pkgs, config, lib, ... }:
with lib;

let
  cfg = config.dag.filesystem;
in {
  options.dag.filesystem = {
    enable = mkOption {
      description = "Enable the filesystem configuration";
      type = types.bool;
      default = true;
    };
    swapfile = mkOption {
      description = "Enable swapfile";
      type = types.bool;
      default = false;
    };
  };

  config = mkIf (cfg.enable) {

    fileSystems = {
      "/" = {
        device = "/dev/disk/by-label/NIXOSROOT";
        fsType = "ext4";
      };
      
      "/boot" = {
        device = "/dev/disk/by-label/NIXOSBOOT";
        fsType = "vfat";
      };
    };

    swapDevices = if cfg.swapfile then
      [ { device = "/swapfile"; size = 2048; } ]
    else
      [ { device = "/dev/disk/by-label/swap"; } ];
  };

}
