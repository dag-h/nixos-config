{ pkgs, config, lib, ... }:
with lib;

let
  cfg = config.dag.musnix;
in {
  options.dag.musnix = {
    enable = mkOption {
      description = "Enable musnix configuration";
      type = types.bool;
      default = false;
    };
  };

  config = mkIf (cfg.enable) {
    musnix = {
      enable = true;
      kernel.realtime = true;
      kernel.packages = pkgs.linuxPackages_latest_rt;
      das_watchdog.enable = true;
    };
  };

}
