{
  description = "Dag's NixOS system configuration";

  inputs = {
    nixpkgs.url = "nixpkgs/nixos-unstable";

    nixpkgs-unstable.url = "nixpkgs/nixpkgs-unstable";

    nixpkgs-dag.url = "github:dag-h/nixpkgs/overwitch-added";

    home-manager = {
      url = "github:nix-community/home-manager/master";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    emacs-overlay = {
      url = github:nix-community/emacs-overlay/d72b6439873ce66a1183d1b28e00a173e8a63ed7;
      inputs.nixpkgs.follows = "nixpkgs";
    };

    musnix = {
      url = github:musnix/musnix;
      inputs.nixpkgs.follows = "nixpkgs";
    };
    
    nixos-hardware = {
      url = github:NixOS/nixos-hardware/master;
    };

    rust-overlay = {
      url = github:oxalica/rust-overlay;
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = inputs@{ self, nixpkgs, nixpkgs-unstable, nixpkgs-dag, home-manager,
                     emacs-overlay, musnix, nixos-hardware, rust-overlay, ... }: 
  let
    inherit (nixpkgs) lib;

    unstable-overlay = final: prev: {
      unstable = import nixpkgs-unstable {
        system = "x86_64-linux";
        config.allowUnfree = true;
      };
    };

    dag-overlay = final: prev: {
      dag = import nixpkgs-dag {
        system = "x86_64-linux";
        config.allowUnfree = true;
      };
    };

    overwitch-overlay = final: prev: {
      inherit (nixpkgs-dag.legacyPackages.x86_64-linux) overwitch overwitch-gtk;
    };

    overlays = [
      emacs-overlay.overlays.default
      unstable-overlay
      dag-overlay
      overwitch-overlay
      rust-overlay.overlays.default
    ];

  in {
    nixosConfigurations = {
      north = let
        system = "x86_64-linux";
        inherit overlays;
        pkgs = import nixpkgs {
          inherit system overlays;
          config.allowUnfree = true;
        };
        util = import ./lib {
          inherit pkgs inputs home-manager system lib overlays;
        };
      in util.host.mkHost rec {
        userName = "dag";
        hostName = "north";
        systemUser = {
          name = "${userName}";
          groups = [ "wheel" "networkmanager" ];
        };
        NICs = [ "enp14s0" "wlp15s0" ];
        availInitrdMods = [ "nvme" "ahci" "xhci_pci" "usbhid" "usb_storage" "sd_mod" ];
        kernelMods = [ "kvm-amd" ];
        kernelParams = [];
        kernelPackage = pkgs.linuxPackages_latest;
        cpuCores = 6;
        videoDrivers = [ "modesetting" ];
        extra-modules = [ musnix.nixosModules.musnix ];
        userSettings = {
          base.enable = true;
          fzf.enable = true;
          gaming.enable = true;
          git = {
            enable = true;
            user = "Dag Harju";
            email = "dagharju@protonmail.com";
          };
          gnome-extra.enable = true;
          kitty.enable = true;
          music.enable = true;
          neovim.enable = true;
          social.enable = true;
          xdg.enable = true;
          zsh.enable = true;
          sway.enable = false;
          alacritty.enable = true;
          zellij.enable = false;
          tmux.enable = true;
          emacs.enable = true;
        };
        systemSettings = {
          amd.enable = true;
          bluetooth.enable = true;
          boot.systemd-boot = true;
          filesystem.swapfile = false;
          fonts.enable = true;
          gnome = {
            enable = true;
            wayland = true;
          };
          pipewire.enable = true;
          steam.enable = true;
          trackball.enable = true;
          v4l2.enable = true;
          virt.enable = true;
          vpn.enable = true;
          wm.enable = false;
        };
        specialArgs = {
          nixpkgs-dag = nixpkgs-dag;
        };
        stateVersion = "23.05";
      };

      compro = let
        system = "x86_64-linux";
        inherit overlays;
        pkgs = import nixpkgs {
          inherit system overlays;
          config.allowUnfree = true;
        };
        util = import ./lib {
          inherit pkgs inputs home-manager system lib overlays;
        };
      in util.host.mkHost rec {
        hostName = "compro";
        userName = "dag";
        systemUser = {
          name = "${userName}";
          groups = [ "wheel" "networkmanager" "adbusers" ];
        };
        NICs = [ "wlp4s0" ];
        availInitrdMods = [ "xhci_pci" "nvme" "usb_storage" "sd_mod" "rtsx_pci_sdmmc" ];
        kernelMods = [ "kvm-intel" ];
        kernelParams = [ ];
        kernelPackage = pkgs.linuxPackages_latest;
        cpuCores = 4;
        wifi = [ "wlp4s0" ];
        extra-modules = [ musnix.nixosModules.musnix ];
        userSettings = {
          base.enable = true;
          fzf.enable = true;
          kitty.enable = true;
          neovim.enable = true;
          zsh.enable = true;
          xdg.enable = true;
          music.enable = true;
          gnome-extra.enable = true;
          gaming.enable = true;
          git = {
            enable = true;
            user = "Dag Harju";
            email = "dagharju@protonmail.com";
          };
          social.enable = true;
          bspwm.enable = false;
          tmux.enable = true;
          emacs.enable = true;
        };
        systemSettings = {
          adb.enable = true;
          wireshark.enable = true;
          fonts.enable = true;
          pipewire.enable = true;
          bluetooth.enable = true;
          gnome = {
            enable = true;
            wayland = true;
          };
          thinkpad.enable = true;
          vpn.enable = true;
          filesystem.swapfile = true;
          printing.enable = true;
          steam.enable = true;
          musnix.enable = false;
          boot = {
            grub = true;
            grub-device = "/dev/nvme0n1";
          };
        };
        specialArgs = {
          nixpkgs-dag = nixpkgs-dag;
        };
        stateVersion = "21.05";
      };
      
      cichli = let
        system = "aarch64-linux";
        inherit overlays;
        pkgs = import nixpkgs {
          inherit system overlays;
          config.allowUnfree = true;
        };
        util = import ./lib {
          inherit pkgs inputs home-manager system lib overlays;
        };
      in util.host.mkHost rec {
        hostName = "cichli";
        userName = "dag";
        systemUser = {
          name = "${userName}";
          groups = [ "wheel" ];
        };
        NICs = [ "wlan0" ];
        availInitrdMods = [ "usbhid" "usb_storage" "vc4" ];
        kernelMods = [ ];
        kernelParams = [
          "8250.nr_uarts=1"
          "console=ttyAMA0,115200"
          "console=tty1"
          # A lot GUI programs need this, nearly all wayland applications
          "cma=128M"
        ];
        kernelPackage = pkgs.linuxPackages_rpi4;
        cpuCores = 4;
        wifi = [ "wlan0" ];
        extra-modules = [ musnix.nixosModules.musnix nixos-hardware.nixosModules.raspberry-pi-4 ];
        userSettings = {
          base.enable = false;
          bspwm.enable = false;
          fzf.enable = false;
          gaming.enable = false;
          git = {
            enable = true;
            user = "Dag Harju";
            email = "dagharju@protonmail.com";
          };
          gnome-extra.enable = false;
          kitty.enable = false;
          music.enable = false;
          neovim.enable = true;
          social.enable = false;
          xdg.enable = false;
          zsh = {
            enable = true;
          };
          rpi.enable = true;
        };
        systemSettings = {
          adb.enable = false;
          bluetooth.enable = false;
          rpi = {
            enable = true;
            autologinUser = userName;
          };
          filesystem.swapfile = true;
          fonts.enable = false;
          gnome.enable = false;
          musnix.enable = false;
          nextcloud.enable = true;
          pipewire.enable = false;
          printing.enable = false;
          steam.enable = false;
          thinkpad.enable = false;
          trackball.enable = false;
          virt.enable = false;
          vpn.enable = false;
          wireshark.enable = false;
          wm.enable = false;
          xfce.enable = false;
        };
        specialArgs = {
          nixpkgs-dag = nixpkgs-dag;
        };
        stateVersion = "22.11";
      };
    };
  };
}
