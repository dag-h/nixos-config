(require 'autothemer)

(autothemer-deftheme
 paraiso-dark "Paraiso light theme"

 ((((class color) (min-colors #xFFFFFF)))

  ;; Color palette
  (paraiso-fg 		"#e7e9db")
  (paraiso-bg 		"#2f1e2e")
  (paraiso-bg2		"#251825")
  (paraiso-black      	"#2f1e2e")
  (paraiso-white      	"#e7e9db")
  (paraiso-cursor     	"#e7e9db")
  (paraiso-grey    	"#8d8687")

  (paraiso-red        	"#ef6155")
  (paraiso-red2        	"#b64a41")

  (paraiso-green      	"#48b685")
  (paraiso-green2      	"#398f68")

  (paraiso-orange     	"#f99b15")
  (paraiso-orange2     	"#bf7710")

  (paraiso-yellow     	"#FDBD26")
  (paraiso-yellow2     	"#c8951e")

  (paraiso-blue       	"#06b6ef")
  (paraiso-blue2       	"#0488b2")

  (paraiso-magenta    	"#815ba4")
  (paraiso-magenta2    	"#5f4379")

  (paraiso-cyan       	"#5bc4bf")
  (paraiso-cyan2       	"#3f8985"))

 ((default 				(:inherit 'fixed-pitch :foreground paraiso-fg :background paraiso-bg))

  ;; font-lock
  (font-lock-keyword-face 		(:foreground paraiso-magenta))
  (font-lock-string-face 		(:foreground paraiso-green))
  (font-lock-comment-face 		(:foreground paraiso-grey))
  (font-lock-builtin-face 		(:foreground paraiso-cyan))
  (font-lock-constant-face 		(:foreground paraiso-yellow :slant 'italic))
  (font-lock-type-face 			(:foreground paraiso-blue))
  (font-lock-variable-name-face 	(:foreground paraiso-red))
  (font-lock-function-name-face 	(:foreground paraiso-red))
  (font-lock-preprocessor-face 		(:foreground paraiso-cyan))

  ;; general
  (cursor  				(:background paraiso-cursor))
  (fringe  				(:foreground paraiso-fg :background paraiso-bg))
  (highlight				(:foreground paraiso-fg :background paraiso-cyan))
  (highlight-numbers-number		(:foreground paraiso-orange))
  (show-paren-match			(:background paraiso-red :foreground paraiso-fg))
  (region				(:background paraiso-grey))
  (link					(:foreground paraiso-blue :underline t))
  (fringe 				(:background paraiso-bg))
  (warning 				(:background paraiso-bg :foreground paraiso-orange))

  ;; org
  (org-imminent-deadline 		(:foreground paraiso-red))
  (org-upcoming-deadline 		(:foreground paraiso-yellow))
  (org-todo 	                	(:foreground paraiso-blue :weight 'bold))
  (org-table 	                	(:inherit 'fixed-pitch :foreground paraiso-cyan))
  (org-done 	        		(:foreground paraiso-green :weight 'bold :strike-through t))
  (org-scheduled 	        	(:foreground paraiso-blue))
  (org-scheduled-previously 	        (:foreground paraiso-red))
  (org-date 	        		(:inherit 'fixed-pitch :foreground paraiso-orange))
  (org-document-title 	        	(:foreground paraiso-fg :height 200))
  (org-document-info-keyword 	        (:inherit 'fixed-pitch :foreground paraiso-grey))
  (org-code 	        		(:inherit 'fixed-pitch :foreground paraiso-blue))
  (org-document-info 	        	(:foreground paraiso-cyan))
  (org-drawer 	        		(:inherit 'org-special-keyword))
  (org-headline-done 	        	(:foreground paraiso-grey))
  (org-ellipsis 	        	(:underline nil :extend t))
  (org-dispatcher-highlight 	        (:background paraiso-green :foreground paraiso-fg))
  (org-latex-and-related 	        (:inherit 'fixed-pitch :foreground paraiso-orange))
  (org-block-begin-line 	        (:inherit 'fixed-pitch :background paraiso-bg2 :extend t))
  (org-block-end-line 	        	(:inherit 'fixed-pitch :background paraiso-bg2 :extend t))
  (org-special-keyword 	        	(:inherit 'fixed-pitch :foreground paraiso-magenta))
  (org-drawer 	        		(:inherit 'org-special-keyword))
  (org-hide 	        		(:foreground paraiso-bg :background paraiso-bg))
  (org-checkbox 	        	(:inherit 'fixed-pitch :height 180))

  (org-level-8				(:inherit 'default :weight 'bold))
  (org-level-7				(:inherit 'org-level-8 :extend t))
  (org-level-6				(:inherit 'org-level-8 :extend t))
  (org-level-5				(:inherit 'org-level-8 :extend t))
  (org-level-4				(:inherit 'org-level-8 :height 1.1))
  (org-level-3				(:inherit 'org-level-8 :height 1.15))
  (org-level-2				(:inherit 'org-level-8 :height 1.25))
  (org-level-1				(:inherit 'org-level-8 :height 1.5))
  (org-block				(:inherit 'fixed-pitch :background paraiso-bg))

  ;; agenda
  (org-agenda-date-today 		(:foreground paraiso-magenta :weight 'bold))
  (org-agenda-date 			(:foreground paraiso-magenta2))
  (org-agenda-structure 		(:foreground paraiso-magenta))
  (org-agenda-calendar-event 		(:foreground paraiso-fg))
  (org-agenda-done 			(:foreground paraiso-green))
  (org-time-grid 			(:foreground paraiso-fg))
  (org-agenda-clocking 			(:background paraiso-orange :foreground paraiso-fg))
  (secondary-selection 			(:background paraiso-bg :foreground paraiso-red))

  ;; modeline
  (mode-line				(:background paraiso-bg2 :foreground paraiso-fg))
  (mode-line-inactive			(:background paraiso-bg2 :foreground paraiso-fg))
  (doom-modeline-spc-face		(:background paraiso-bg2 :foreground paraiso-fg))
  (doom-modeline-evil-insert-state	(:background paraiso-bg2 :foreground paraiso-red))
  (doom-modeline-evil-normal-state	(:background paraiso-bg2 :foreground paraiso-fg))
  (doom-modeline-evil-visual-state	(:background paraiso-bg2 :foreground paraiso-yellow))
  (doom-modeline-buffer-modified	(:background paraiso-bg2 :foreground paraiso-red))
  (doom-modeline-bar			(:background paraiso-fg :foreground paraiso-fg))
  (doom-modeline-bar-inactive		(:background paraiso-bg2 :foreground paraiso-bg2))
  (doom-modeline-info 			(:foreground paraiso-green))
  (doom-modeline-project-dir 		(:foreground paraiso-green :weight 'normal))
  (doom-modeline-buffer-path 		(:foreground paraiso-fg :weight 'normal))
  (doom-modeline-buffer-file 		(:foreground paraiso-fg :weight 'normal))

  ;; Misc
  (minibuffer-prompt			(:background paraiso-bg :foreground paraiso-blue :box nil))
  (rustic-question-mark			(:background paraiso-bg :foreground paraiso-yellow))
  (orderless-match-face-0		(:foreground paraiso-blue))
  (orderless-match-face-1		(:foreground paraiso-red))
  (orderless-match-face-2		(:foreground paraiso-green))
  (orderless-match-face-3		(:foreground paraiso-yellow))
  (header-line				(:background paraiso-bg))
  (flycheck-fringe-warning		(:background paraiso-bg :foreground paraiso-red))
  (bookmark-face			(:background paraiso-bg :foreground paraiso-green))
  ))

(provide-theme 'paraiso-dark)
