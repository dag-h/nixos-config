(require 'autothemer)

(autothemer-deftheme
 dag "My theme"

 ((((class color) (min-colors #xFFFFFF)))

  ;; color palette
  (dag-foreground 	"#37474F")
  (dag-background 	"#FFFFFF")
  (dag-highlight 	"#FAFAFA")
  (dag-subtle 		"#ECEFF1")
  (dag-faded 		"#90A4AE")
  (dag-salient 		"#673ab7")
  (dag-strong 		"#263238")
  (dag-popout 		"#FFAB91")
  (dag-critical 	"#FF6F00"))

 (
  ;; base
  (default
   (:inherit 'fixed-pitch :foreground dag-foreground :background dag-background :weight 'normal))
  (cursor
   (:foreground dag-background :background dag-foreground))
  (mouse
   (:foreground dag-foreground :background dag-background))
  (highlight
   (:background dag-highlight))
  (dag-default
   (:foreground dag-foreground))
  (dag-default-i
   (:foreground dag-background :background dag-foreground))
  (dag-title
   (:inherit 'default))
  ;; (dag-title
  ;;  (:inherit 'default :font "Roboto Slab" :family "Roboto Slab"))
  (dag-subtle
   (:background dag-subtle))
  (dag-subtle-i
   (:foreground dag-subtle))
  (dag-faded
   (:foreground dag-faded))
  (dag-faded-i
   (:background dag-faded))
  (dag-salient
   (:foreground dag-salient))
  (dag-salient-i
   (:foreground dag-background :background dag-salient))
  (dag-strong
   (:foreground dag-strong :weight 'bold))
  (dag-strong-i
   (:foreground dag-background :background dag-strong))
  (dag-popout
   (:foreground dag-popout))
  (dag-popout-i
   (:foreground dag-background :background dag-popout))
  (dag-critical
   (:foreground dag-critical))
  (dag-critical-i
   (:foreground dag-background :background dag-critical))
  (dag-svg
   (:inherit 'dag-strong :font "Roboto Slab" :family "Roboto Slab" :height 200))

  ;; structural
  (bold
   (:inherit 'dag-strong))
  (italic
   (:inherit 'dag-faded))
  (bold-italic
   (:inherit 'dag-strong))
  (region
   (:inherit 'dag-subtle :distant-foreground nil))
  (fringe
   (:inherit 'dag-faded))
  (hl-line
   (:inherit 'highlight))
  (link
   (:inherit 'dag-salient :underline t))
  (fixed-pitch
   (:inherit 'default))
  (fixed-pitch-serif
   (:inherit 'default))

  ;; semantic
  (shadow
   (:inherit 'dag-faded))
  (success
   (:inherit 'dag-salient))
  (warning
   (:inherit 'dag-popout))
  (error
   (:inherit 'dag-critical))
  (match
   (:inherit 'dag-popout))

  ;; general
  (buffer-menu-buffer
   (:inherit 'dag-strong))
  (minibuffer-prompt
   (:inherit 'dag-strong))
  (isearch
   (:inherit 'dag-strong))
  (isearch-fail
   (:inherit 'dag-faded))
  (isearch-fail
   (:inherit 'dag-faded))
  (show-paren-match
   (:inherit 'dag-strong))
  (show-paren-mismatch
   (:inherit 'dag-critical))
  (lazy-highlight
   (:inherit 'dag-subtle))
  (trailing-whitespace
   (:inherit 'dag-subtle))
  (secondary-selection
   (:inherit 'dag-subtle))
  (completions-annotations
   (:inherit 'dag-faded))
  (completions-common-part
   (:inherit 'dag-strong))
  (completions-first-difference
   (:inherit 'default))
  (tooltip
   (:inherit 'dag-subtle))
  (read-multiple-choice-face
   (:inherit 'dag-strong))
  (nobreak-hyphen
   (:inherit 'dag-popout))
  (nobreak-space
   (:inherit 'dag-popout))
  (help-argument-name
   (:inherit 'dag-faded))
  (tabulated-list-fake-header
   (:inherit 'dag-strong))
  (tool-bar
   (:inherit 'dag-faded-i))

  ;; window divider
  (window-divider
   (:foreground dag-background))
  (window-divider-first-pixel
   (:inherit 'window-divider))
  (window-divider-last-pixel
   (:inherit 'window-divider))
  (vertical-border
   (:inherit 'window-divider))

  ;; tab bar
  (tab-bar
   (:inherit 'default))
  (tab-bar-tab
   (:inherit 'default))
  (tab-bar-tab-inactive
   (:inherit 'dag-faded))
  (tab-line
   (:inherit 'default))

  ;; line numbers
  (line-number
   (:inherit 'dag-faded))
  (line-number-current-line
   (:inherit nil))
  (line-number-major-tick
   (:inherit 'dag-faded))
  (line-number-minor-tick
   (:inherit 'dag-faded))

  ;; font-lock
  (font-lock-comment-face
   (:inherit 'dag-faded))
  (font-lock-doc-face
   (:inherit 'dag-faded))
  (font-lock-string-face
   (:inherit 'dag-faded))
  (font-lock-constant-face
   (:inherit 'dag-salient))
  (font-lock-warning-face
   (:inherit 'dag-popout))
  (font-lock-function-name-face
   (:inherit 'dag-strong))
  (font-lock-variable-name-face
   (:inherit '(dag-strong dag-salient)))
  (font-lock-builtin-face
   (:inherit 'dag-salient))
  (font-lock-type-face
   (:inherit 'dag-salient))
  (font-lock-keyword-face
   (:inherit 'dag-salient))
  (font-lock-preprocessor-face
   (:inherit 'dag-salient))

  ;; tree-sitter
  (tree-sitter-hl-face:function.method
   (:inherit 'font-lock-function-name-face :slant 'normal))
  (tree-sitter-hl-face:property
   (:inherit 'dag-default :slant 'normal))
  (tree-sitter-hl-face:constructor
   (:inherit 'font-lock-type-face :weight 'normal))

  ;; custom edit
  (widget-field
   (:inherit 'dag-subtle))
  (widget-button
   (:inherit 'dag-strong))
  (widget-single-line-field
   (:inherit 'dag-subtle))
  (custom-group-subtitle
   (:inherit 'dag-strong))
  (custom-group-tag
   (:inherit 'dag-strong))
  (custom-group-tag-1
   (:inherit 'dag-strong))
  (custom-comment
   (:inherit 'dag-faded))
  (custom-comment-tag
   (:inherit 'dag-faded))
  (custom-changed
   (:inherit 'dag-salient))
  (custom-modified
   (:inherit 'dag-salient))
  (custom-face-tag
   (:inherit 'dag-strong))
  (custom-variable-tag
   (:inherit 'dag-strong))
  (custom-invalid
   (:inherit 'dag-popout))
  (custom-visibility
   (:inherit 'dag-salient))
  (custom-state
   (:inherit 'dag-salient))
  (custom-link
   (:inherit 'dag-salient))
  (custom-variable-obsolete
   (:inherit 'dag-faded))

  ;; buttons
  (custom-button
   (:foreground dag-faded :background dag-highlight :box nil))
  (custom-button-mouse
   (:foreground dag-foreground :background dag-subtle :box nil))
  (custom-button-pressed
   (:foreground dag-background :background dag-foreground :box nil))

  ;; packages
  (package-description
   (:inherit 'dag-default))
  (package-help-section-name
   (:inherit 'dag-default))
  (package-name
   (:inherit 'dag-salient))
  (package-status-avail-obso
   (:inherit 'dag-faded))
  (package-status-available
   (:inherit 'dag-default))
  (package-status-built-in
   (:inherit 'dag-salient))
  (package-status-dependency
   (:inherit 'dag-salient))
  (package-status-disabled
   (:inherit 'dag-faded))
  (package-status-external
   (:inherit 'dag-default))
  (package-status-held
   (:inherit 'dag-default))
  (package-status-incompat
   (:inherit 'dag-faded))
  (package-status-installed
   (:inherit 'dag-salient))
  (package-status-new
   (:inherit 'dag-default))
  (package-status-unsigned
   (:inherit 'dag-default))

  ;; info
  (info-node
   (:inherit 'dag-strong))
  (info-menu-header
   (:inherit 'dag-strong))
  (info-header-node
   (:inherit 'dag-default))
  (info-index-match
   (:inherit 'dag-salient))
  (Info-quoted
   (:inherit 'dag-faded))
  (info-title-1
   (:inherit 'dag-strong))
  (info-title-2
   (:inherit 'dag-strong))
  (info-title-3
   (:inherit 'dag-strong))
  (info-title-4
   (:inherit 'dag-strong))

  ;; helpful
  (helpful-heading
   (:inherit 'dag-strong))

  ;; modeline
  (mode-line
   (:foreground dag-foreground :background dag-subtle :box (:line-width 5 :color dag-subtle :style nil)))
  (mode-line-inactive
   (:foreground dag-foreground :background dag-subtle :box (:line-width 5 :color dag-subtle :style nil)))
  (doom-modeline-spc-face
   (:inherit 'mode-line))
  (doom-modeline-evil-insert-state
   (:inherit 'mode-line))
  (doom-modeline-evil-normal-state
   (:inherit 'mode-line))
  (doom-modeline-evil-visual-state
   (:inherit 'mode-line))
  (doom-modeline-buffer-modified
   (:inherit 'mode-line))
  (doom-modeline-bar
   (:foreground dag-foreground :background dag-faded))
  (doom-modeline-bar-inactive
   (:foreground dag-foreground :background dag-subtle))
  (doom-modeline-info
   (:inherit 'mode-line))
  (doom-modeline-project-dir
   (:inherit 'mode-line))
  (doom-modeline-buffer-path
   (:inherit 'mode-line))
  (doom-modeline-buffer-file
   (:inherit 'mode-line :inherit 'dag-strong))
  (doom-modeline-buffer-major-mode
   (:inherit 'mode-line))
  (doom-modeline-warning
   (:inherit 'mode-line))
  (doom-modeline-lsp-running
   (:inherit 'mode-line))
  (doom-modeline-lsp-warning
   (:inherit 'mode-line))
  (doom-modeline-lsp-success
   (:inherit 'mode-line))
  (doom-modeline-notification
   (:inherit 'mode-line))
  (doom-modeline-repl-warning
   (:inherit 'mode-line))
  (doom-modeline-repl-warning
   (:inherit 'mode-line))
  (doom-modeline-battery-warning
   (:inherit 'mode-line))
  (header-line
   (:foreground dag-foreground :background dag-subtle :inherit nil :box nil))

  ;; diff
  (diff-header
   (:inherit 'dag-faded))
  (diff-file-header
   (:inherit 'dag-strong))
  (diff-context
   (:inherit 'dag-default))
  (diff-removed
   (:inherit 'dag-faded))
  (diff-changed
   (:inherit 'dag-popout))
  (diff-added
   (:inherit 'dag-salient))
  (diff-refine-added
   (:inherit '(dag-salient dag-strong)))
  (diff-refine-changed
   (:inherit 'dag-popout))
  (diff-refine-removed
   (:inherit 'dag-faded :strike-through t))

  ;; vertico
  (vertico-current
   (:inherit '(dag-strong dag-subtle)))
  (vertico-group-separator
   (:inherit 'dag-faded))
  (vertico-group-title
   (:inherit 'dag-faded))
  (vertico-multiline
   (:inherit 'dag-faded))

  ;; message
  (message-cited-text-1
   (:inherit 'dag-faded))
  (message-cited-text-2
   (:inherit 'dag-faded))
  (message-cited-text-3
   (:inherit 'dag-faded))
  (message-cited-text-4
   (:inherit 'dag-faded))
  (message-cited-text
   (:inherit 'dag-faded))
  (message-header-cc
   (:inherit 'dag-default))
  (message-header-name
   (:inherit 'dag-strong))
  (message-header-newsgroups
   (:inherit 'dag-default))
  (message-header-other
   (:inherit 'dag-default))
  (message-header-subject
   (:inherit 'dag-salient))
  (message-header-to
   (:inherit 'dag-salient))
  (message-header-xheader
   (:inherit 'dag-default))
  (message-mml
   (:inherit 'dag-popout))
  (message-separator
   (:inherit 'dag-faded))

  ;; orderless
  (orderless-match-face-0
   (:inherit '(dag-salient dag-strong)))
  (orderless-match-face-1
   (:inherit 'dag-strong))
  (orderless-match-face-2
   (:inherit 'dag-strong))
  (orderless-match-face-3
   (:inherit 'dag-strong))

  ;; outline
  (outline-1
   (:inherit 'dag-strong :height 1.3))
  (outline-2
   (:inherit 'outline-1))
  (outline-3
   (:inherit 'outline-1))
  (outline-4
   (:inherit 'outline-1))
  (outline-5
   (:inherit 'outline-1))
  (outline-6
   (:inherit 'outline-1))
  (outline-7
   (:inherit 'outline-1))
  (outline-8
   (:inherit 'outline-1))

  ;; flyspell
  (flyspell-duplicate
   (:inherit 'dag-popout :underline t))
  (flyspell-incorrect
   (:inherit 'dag-popout :underline t))

  ;; agenda
  (org-agenda-calendar-event
   (:inherit 'dag-default))
  (org-agenda-calendar-sexp
   (:inherit 'dag-salient))
  (org-agenda-clocking
   (:inherit 'dag-faded))
  (org-agenda-column-dateline
   (:inherit 'dag-faded))
  (org-agenda-current-time
   (:inherit 'dag-strong))
  (org-agenda-date
   (:inherit 'dag-salient))
  (org-agenda-date-today
   (:inherit '(dag-salient dag-strong)))
  (org-agenda-date-weekend
   (:inherit 'dag-faded))
  (org-agenda-diary
   (:inherit 'dag-faded))
  (org-agenda-dimmed-todo-face
   (:inherit 'dag-faded))
  (org-agenda-done
   (:inherit 'dag-faded))
  (org-agenda-filter-category
   (:inherit 'dag-faded))
  (org-agenda-filter-effort
   (:inherit 'dag-faded))
  (org-agenda-filter-regexp
   (:inherit 'dag-faded))
  (org-agenda-filter-tags
   (:inherit 'dag-faded))
  (org-agenda-property-face
   (:inherit 'dag-faded))
  (org-agenda-restriction-lock
   (:inherit 'dag-faded))
  (org-agenda-structure
   (:inherit 'dag-strong))

  ;; org
  (org-archived
   (:inherit 'dag-faded))
  (org-block
   (:inherit 'highlight))
  (org-block-begin-line
   (:inherit 'dag-faded))
  (org-block-end-line
   (:inherit 'dag-faded))
  (org-checkbox
   (:inherit 'dag-faded))
  (org-checkbox-statistics-done
   (:inherit 'dag-faded))
  (org-checkbox-statistics-todo
   (:inherit 'dag-faded))
  (org-clock-overlay
   (:inherit 'dag-faded))
  (org-code
   (:inherit 'dag-salient))
  (org-column
   (:inherit 'dag-faded))
  (org-column-title
   (:inherit 'dag-faded))
  (org-date
   (:inherit 'dag-faded))
  (org-date-selected
   (:inherit 'dag-faded))
  (org-default
   (:inherit 'dag-faded))
  (org-document-info
   (:inherit 'dag-faded :height 160))
  (org-document-info-keyword
   (:inherit 'dag-faded))
  (org-document-title
   (:inherit 'dag-default :family "Roboto Slab" :height 300))
  (org-done
   (:inherit 'dag-faded))
  (org-drawer
   (:inherit 'dag-faded))
  (org-ellipsis
   (:inherit 'dag-faded))
  (org-footnote
   (:inherit 'dag-faded))
  (org-formula
   (:inherit 'dag-faded))
  (org-headline-done
   (:inherit 'dag-faded))
  (org-latex-and-related
   (:inherit 'dag-faded))
  
  (org-level-1
   (:inherit 'outline-1))
  (org-level-2
   (:inherit 'outline-2))
  (org-level-3
   (:inherit 'outline-3))
  (org-level-4
   (:inherit 'outline-4))
  (org-level-5
   (:inherit 'outline-5))
  (org-level-6
   (:inherit 'outline-6))
  (org-level-7
   (:inherit 'outline-7))
  (org-level-8
   (:inherit 'outline-8))
  
  (org-link
   (:inherit 'dag-salient :underline t))
  (org-list-dt
   (:inherit 'dag-faded))
  (org-macro
   (:inherit 'dag-faded))
  (org-meta-line
   (:inherit 'dag-faded))
  (org-mode-line-clock
   (:inherit 'dag-faded))
  (org-mode-line-clock-overrun
   (:inherit 'dag-faded))
  (org-priority
   (:inherit 'dag-faded))
  (org-property-value
   (:inherit 'dag-faded))
  (org-quote
   (:inherit 'dag-faded))
  (org-scheduled
   (:inherit 'dag-faded))
  (org-scheduled-previously
   (:inherit 'dag-faded))
  (org-scheduled-today
   (:inherit 'dag-faded))
  (org-sexp-date
   (:inherit 'dag-faded))
  (org-special-keyword
   (:inherit 'dag-faded))
  (org-table
   (:inherit 'dag-faded))
  (org-tag
   (:inherit 'dag-popout))
  (org-tag-group
   (:inherit 'dag-faded))
  (org-target
   (:inherit 'dag-faded))
  (org-time-grid
   (:inherit 'dag-faded))
  (org-todo
   (:inherit 'dag-salient))
  (org-upcoming-deadline
   (:inherit 'dag-popout))
  (org-verbatim
   (:inherit 'dag-popout))
  (org-verse
   (:inherit 'dag-faded))
  (org-warning
   (:inherit 'dag-popout))
  ;; (org-imminent-deadline
  ;;  (:foreground dag-fg :background dag-red))
  ;; (org-meta-line
  ;;  (:foreground dag-fg2))
  ;; (org-ellipsis
  ;;  (:font "Roboto Slab" :family "Roboto Slab" :height 0.7 :foreground dag-faded))
  ;; (org-dispatcher-highlight
  ;;  (:foreground dag-fg :background dag-green))
  ;; (org-hide
  ;;  (:foreground dag-bg :background dag-bg))
  ;; (org-indent
  ;;  (:inherit ('fixed-pitch 'org-hide)))

  ;; marginalia
  (marginalia-archive
   (:inherit 'dag-faded))
  (marginalia-char
   (:inherit 'dag-faded))
  (marginalia-date
   (:inherit 'dag-faded))
  (marginalia-documentation
   (:inherit 'dag-faded))
  (marginalia-file-name
   (:inherit 'dag-faded))
  (marginalia-file-owner
   (:inherit 'dag-faded))
  (marginalia-file-priv-dir
   (:inherit 'dag-faded))
  (marginalia-file-priv-exec
   (:inherit 'dag-faded))
  (marginalia-file-priv-link
   (:inherit 'dag-faded))
  (marginalia-file-priv-no
   (:inherit 'dag-faded))
  (marginalia-file-priv-other
   (:inherit 'dag-faded))
  (marginalia-file-priv-other
   (:inherit 'dag-faded))
  (marginalia-file-priv-rare
   (:inherit 'dag-faded))
  (marginalia-file-priv-read
   (:inherit 'dag-faded))
  (marginalia-file-priv-write
   (:inherit 'dag-faded))
  (marginalia-function
   (:inherit 'dag-faded))
  (marginalia-installed
   (:inherit 'dag-faded))
  (marginalia-key
   (:inherit 'dag-faded))
  (marginalia-lighter
   (:inherit 'dag-faded))
  (marginalia-list
   (:inherit 'dag-faded))
  (marginalia-mode
   (:inherit 'dag-faded))
  (marginalia-modified
   (:inherit 'dag-faded))
  (marginalia-null
   (:inherit 'dag-faded))
  (marginalia-number
   (:inherit 'dag-faded))
  (marginalia-off
   (:inherit 'dag-faded))
  (marginalia-on
   (:inherit 'dag-faded))
  (marginalia-size
   (:inherit 'dag-faded))
  (marginalia-string
   (:inherit 'dag-faded))
  (marginalia-symbol
   (:inherit 'dag-faded))
  (marginalia-true
   (:inherit 'dag-faded))
  (marginalia-type
   (:inherit 'dag-faded))
  (marginalia-value
   (:inherit 'dag-faded))
  (marginalia-version
   (:inherit 'dag-faded))

  ;; imenu
  (imenu-list-entry-face
   (:inherit 'dag-default))
  (imenu-list-entry-face-0
   (:inherit 'dag-strong))
  (imenu-list-entry-face-1
   ())
  (imenu-list-entry-face-2
   ())
  (imenu-list-entry-face-3
   ())
  (imenu-list-entry-subalist-face-0
   (:inherit 'dag-strong))
  (imenu-list-entry-subalist-face-1
   ())
  (imenu-list-entry-subalist-face-2
   ())
  (imenu-list-entry-subalist-face-3
   ())

  ;; restructured text
  (rst-adornment
   (:inherit 'dag-faded))
  (rst-block
   (:inherit 'dag-default))
  (rst-comment
   (:inherit 'dag-faded))
  (rst-definition
   (:inherit 'dag-salient))
  (rst-directive
   (:inherit 'dag-salient))
  (rst-emphasis1
   (:inherit 'dag-faded))
  (rst-emphasis2
   (:inherit 'dag-strong))
  (rst-external
   (:inherit 'dag-salient))
  (rst-level-1
   (:inherit 'dag-strong))
  (rst-level-2
   (:inherit 'dag-strong))
  (rst-level-3
   (:inherit 'dag-strong))
  (rst-level-4
   (:inherit 'dag-strong))
  (rst-level-5
   (:inherit 'dag-strong))
  (rst-level-6
   (:inherit 'dag-strong))
  (rst-literal
   (:inherit 'dag-salient))
  (rst-transition
   (:inherit 'dag-default))

  ;; SHR
  (shr-abbreviation
   (:inherit 'dag-popout))
  (shr-h1
   (:inherit 'dag-strong))
  (shr-h2
   (:inherit 'dag-strong))
  (shr-h3
   (:inherit 'dag-strong))
  (shr-h4
   (:inherit 'dag-strong))
  (shr-h5
   (:inherit 'dag-strong))
  (shr-h6
   (:inherit 'dag-strong))
  (shr-link
   (:inherit 'dag-salient))
  (shr-selected-link
   (:inherit '(dag-salient dag-subtle)))
  (shr-strike-through
   (:inherit 'dag-faded))

  ;; magit
  (magit-blame-highlight
   (:inherit 'highlight))
  (magit-diff-added-highlight
   (:inherit '(highlight dag-salient dag-strong)))
  (magit-diff-base-highlight
   (:inherit 'highlight))
  (magit-diff-context-highlight
   (:inherit '(highlight dag-faded)))
  (magit-diff-file-heading-highlight
   (:inherit '(highlight dag-strong)))
  (magit-diff-hunk-heading-highlight
   (:inherit 'dag-default))
  (magit-diff-our-highlight
   (:inherit 'highlight))
  (magit-diff-removed-highlight
   (:inherit '(highlight dag-popout dag-strong)))
  (magit-diff-revision-summary-highlight
   ())
  (magit-diff-their-highlight
   (:inherit 'highlight))
  (magit-section-highlight
   (:inherit 'highlight))
  (magit-blame-heading
   (:inherit '(dag-subtle dag-strong)))
  (magit-diff-conflict-heading
   (:inherit '(dag-subtle dag-strong)))
  (magit-diff-file-heading
   (:inherit 'dag-strong))
  (magit-diff-hunk-heading
   (:inherit '(dag-subtle dag-default)))
  (magit-diff-lines-heading
   (:inherit '(dag-subtle dag-strong)))
  (magit-section-heading
   (:inherit '(dag-salient dag-strong)))
  (magit-bisect-bad
   (:inherit 'dag-default))
  (magit-bisect-good
   (:inherit 'dag-default))
  (magit-bisect-skip
   (:inherit 'dag-default))
  (magit-bisect-date
   (:inherit 'dag-default))
  (magit-bisect-dimmed
   (:inherit 'dag-default))
  (magit-bisect-hash
   (:inherit 'dag-faded))
  (magit-blame-margin
   (:inherit 'dag-default))
  (magit-blame-name
   (:inherit 'dag-default))
  (magit-blame-summary
   (:inherit 'dag-default))
  (magit-branch-current
   (:inherit '(dag-strong dag-salient)))
  (magit-branch-local
   (:inherit 'dag-salient))
  (magit-branch-remote
   (:inherit 'dag-salient))
  (magit-branch-remote-head
   (:inherit 'dag-salient))
  (magit-branch-upstream
   (:inherit 'dag-salient))
  (magit-cherry-equivalent
   (:inherit 'dag-default))
  (magit-cherry-unmatched
   (:inherit 'dag-default))
  (magit-diff-added
   (:inherit '(highlight dag-salient dag-strong)))
  (magit-diff-base
   (:inherit 'dag-default))
  (magit-diff-context
   (:inherit '(highlight dag-faded)))
  (magit-diff-file-heading-selection
   (:inherit 'dag-default))
  (magit-diff-hunk-heading-selection
   (:inherit 'dag-default))
  (magit-diff-hunk-region
   (:inherit 'dag-default))
  (magit-diff-lines-boundary
   (:inherit 'dag-default))
  (magit-diff-our
   (:inherit 'dag-default))
  (magit-diff-removed
   (:inherit '(highlight dag-popout dag-strong)))
  (magit-diff-revision-summary
   (:inherit 'dag-popout))
  (magit-diff-their
   (:inherit 'dag-default))
  (magit-diff-whitespace-warning
   (:inherit 'dag-subtle))
  (magit-diffstat-added
   (:inherit 'dag-default))
  (magit-diffstat-removed
   (:inherit 'dag-default))
  (magit-dimmed
   (:inherit 'dag-faded))
  (magit-filename
   (:inherit 'dag-default))
  (magit-hash
   (:inherit 'dag-faded))
  (magit-head
   (:inherit 'dag-default))
  (magit-header-line
   (:inherit 'dag-default))
  (magit-header-line-key
   (:inherit 'dag-default))
  (magit-header-line-log-select
   (:inherit 'dag-default))
  (magit-keyword
   (:inherit 'dag-default))
  (magit-keyword-squash
   (:inherit 'dag-default))
  (magit-log-author
   (:inherit 'dag-default))
  (magit-log-date
   (:inherit 'dag-default))
  (magit-log-graph
   (:inherit 'dag-default))
  (magit-mode-line-process
   (:inherit 'dag-default))
  (magit-mode-line-process-error
   (:inherit 'dag-critical))
  (magit-process-ng
   (:inherit 'dag-default))
  (magit-process-ok
   (:inherit 'dag-default))
  (magit-reflog-amend
   (:inherit 'dag-default))
  (magit-reflog-checkout
   (:inherit 'dag-default))
  (magit-reflog-cherry-pick
   (:inherit 'dag-default))
  (magit-reflog-commit
   (:inherit 'dag-default))
  (magit-reflog-merge
   (:inherit 'dag-default))
  (magit-reflog-other
   (:inherit 'dag-default))
  (magit-reflog-rebase
   (:inherit 'dag-default))
  (magit-reflog-remote
   (:inherit 'dag-default))
  (magit-reflog-reset
   (:inherit 'dag-default))
  (magit-refname
   (:inherit 'dag-default))
  (magit-refname-pullreq
   (:inherit 'dag-default))
  (magit-refname-stash
   (:inherit 'dag-default))
  (magit-refname-wip
   (:inherit 'dag-default))
  (magit-section-heading-selection
   (:inherit 'dag-default))
  (magit-section-secondary-heading
   (:inherit 'dag-default))
  (magit-sequence-done
   (:inherit 'dag-default))
  (magit-sequence-drop
   (:inherit 'dag-default))
  (magit-sequence-exec
   (:inherit 'dag-default))
  (magit-sequence-head
   (:inherit 'dag-default))
  (magit-sequence-onto
   (:inherit 'dag-default))
  (magit-sequence-part
   (:inherit 'dag-default))
  (magit-sequence-pick
   (:inherit 'dag-default))
  (magit-sequence-stop
   (:inherit 'dag-default))
  (magit-signature-bad
   (:inherit 'dag-default))
  (magit-signature-error
   (:inherit 'dag-default))
  (magit-signature-expired
   (:inherit 'dag-default))
  (magit-signature-expired-key
   (:inherit 'dag-default))
  (magit-signature-good
   (:inherit 'dag-default))
  (magit-signature-revoked
   (:inherit 'dag-default))
  (magit-signature-untrusted
   (:inherit 'dag-default))
  (magit-tag
   (:inherit 'dag-strong))

  ;; misc
  (rustic-question-mark
   (:foreground dag-foreground :background dag-popout))
  (flycheck-fringe-warning
   (:foreground dag-foreground :background dag-popout))
  (flycheck-error-list-error
   (:foreground dag-foreground))

  (cider-result-overlay-face
   (:inherit 'dag-faded))
  (help-key-binding
   (:inherit 'dag-subtle))

  ;; (term-color-green
  ;;  (:foreground dag-dark_green))
  ;; (term-color-red
  ;;  (:foreground dag-dark_red))
  ;; (term-color-blue
  ;;  (:foreground dag-dark_blue))
  ;; (term-color-cyan
  ;;  (:foreground dag-dark_blue))
  ;; (term-color-cyan
  ;;  (:foreground dag-dark_blue))
  ;; (term-color-black
  ;;  (:foreground dag-fg))
  ;; (term-color-white
  ;;  (:foreground dag-bg))
  ;; (term-color-yellow
  ;;  (:foreground dag-dark_yellow))
  ;; (term-color-magenta
  ;;  (:foreground dag-dark_magenta))


  ;; lsp
  (lsp-modeline-code-actions-face
   (:foreground dag-foreground))
  (lsp-flycheck-info-unnecessary-face
   (:foreground dag-foreground :underline nil))
  (lsp-ui-sideline-global
   (:foreground dag-foreground :underline nil :height 0.5))
  (lsp-rust-analyzer-inlay-face
   (:inherit 'dag-faded :slant 'italic))

  ;; nix
  (nix-antiquote-face
   (:inherit 'dag-salient))

  ;; vterm
  (vterm-color-blue
   (:inherit 'dag-salient))
  (vterm-color-red
   (:inherit 'dag-critical))
  (vterm-color-green
   (:inherit 'dag-salient))
  (vterm-color-yellow
   (:inherit 'dag-popout))
  (vterm-color-magenta
   (:inherit 'dag-salient))
  (vterm-color-cyan
   (:inherit 'dag-salient))
  (vterm-color-black
   (:inherit 'dag-foreground))
  (vterm-color-white
   (:inherit 'dag-background))

  ;; corfu
  (corfu-border
   (:inherit 'dag-default))
  (corfu-bar
   (:background dag-foreground))
  (corfu-current
   (:background dag-faded :inherit 'dag-strong))
  (corfu-default
   (:inherit 'dag-subtle))
  (corfu-deprecated
   (:inherit 'corfu-default :strike-through t))
  (corfu-annotations
   (:inherit 'dag-salient))
  
  ;; company tooltip
  (company-tooltip
   (:inherit 'dag-subtle))
  (company-tooltip-mouse
   (:inherit 'dag-faded-i))
  (company-tooltip-selection
   (:inherit '(dag-faded-i dag-strong)))
  (company-tooltip-annotation-selection
   (:inherit 'dag-faded-i :weight 'normal))

  (company-scrollbar-fg
   (:inherit 'dag-default-i))
  (company-scrollbar-bg
   (:inherit 'dag-faded-i))

  (company-tooltip-scrollbar-thumb
   (:inherit 'dag-default-i))
  (company-tooltip-scrollbar-track
   (:inherit 'dag-faded-i))

  (company-tooltip-common
   (:inherit 'dag-strong))
  (company-tooltip-common-selection
   (:inherit 'dag-salient-i :weight 'normal))

  (company-tooltip-annotation
   (:inherit 'dag-default))
  (company-tooltip-annotation-selection
   (:inherit 'dag-subtle))

  (meow-beacon-fake-selection
   (:inherit 'dag-highlight))
  
  ))

(provide-theme 'dag)
