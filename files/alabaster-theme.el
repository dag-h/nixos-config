(require 'autothemer)

(autothemer-deftheme
 alabaster-dag "Alabaster"

 ((((class color) (min-colors #xFFFFFF)))

  ;; color palette
  (dag-active 		"#007ACC")
  (dag-fg 		"#000")
  (dag-fg2		"#535353")
  (dag-bg 		"#fff")
  (dag-bg2 		"#f2f2f2")
  (dag-bg3 		"#e6e6e6")
  (dag-blue 		"#DBF1FF")
  (dag-dark_blue 	"#2a6b94")
  (dag-green 		"#F1FADF")
  (dag-dark_green 	"#799e2e")
  (dag-red 		"#FFE0E0")
  (dag-dark_red 	"#9b3a3a")
  (dag-magenta 		"#F9E0FF")
  (dag-dark_magenta 	"#7f448d")
  (dag-yellow 		"#FFFABC")
  (dag-dark_yellow 	"#cdc01d")
  (dag-orange 		"#FFBC5D")
  (dag-dark_orange 	"#d7891b")
  (dag-purple 		"#7A3E9D"))

 ((default 				(:inherit 'fixed-pitch :foreground dag-fg :background dag-bg :weight 'normal))

  (dag/title-font		        (:font "Roboto Slab" :family "Roboto Slab"))
  (dag/ellipsis-font		        (:underline nil :font "Roboto Slab" :family "Roboto Slab" :height 0.7 :foreground dag-bg3))

  ;; font-lock
  (font-lock-keyword-face
   (:foreground dag-purple))
  (font-lock-string-face
   (:foreground dag-fg :background dag-green))
  (font-lock-comment-face
   (:foreground dag-fg :background dag-yellow))
  (font-lock-builtin-face
   (:foreground dag-fg :background dag-bg))
  (font-lock-constant-face
   (:foreground dag-purple))
  (font-lock-type-face
   (:foreground dag-fg :background dag-bg))
  (font-lock-variable-name-face
   (:foreground dag-fg :background dag-blue))
  (font-lock-function-name-face
   (:foreground dag-fg :background dag-blue))
  (font-lock-preprocessor-face
   (:foreground dag-fg :background dag-bg))

  ;; general
  (cursor
   (:foreground dag-bg :background dag-fg))
  (fringe
   (:foreground dag-fg :background dag-bg))
  (highlight
   (:foreground dag-fg :background dag-bg2))
  (show-paren-match
   (:foreground dag-fg :background dag-bg2))
  (region
   (:foreground dag-fg :background dag-bg2))
  (link
   (:foreground dag-dark_blue :underline t))
  (warning
   (:foreground dag-fg :background dag-orange))
  (success
   (:foreground dag-dark_green))
  (window-divider
   (:foreground dag-bg :background dag-bg))
  (window-divider-last-pixel
   (:inherit 'window-divider))
  (window-divider-first-pixel
   (:inherit 'window-divider))

  ;; org
  (org-imminent-deadline
   (:foreground dag-fg :background dag-red))
  (org-upcoming-deadline
   (:foreground dag-fg :background dag-yellow))
  (org-todo
   (:foreground dag-fg :background dag-blue :weight 'bold))
  (org-tag
   (:foreground dag-fg2 :background dag-bg))
  (org-table
   (:inherit 'fixed-pitch :foreground dag-fg))
  (org-done
   (:foreground dag-fg :background dag-green :weight 'bold :strike-through t))
  (org-scheduled
   (:inherit 'default))
  (org-scheduled-today
   (:inherit 'default))
  (org-scheduled-previously
   (:foreground dag-fg :background dag-red))
  (org-date
   (:inherit 'fixed-pitch :foreground dag-fg2))
  (org-document-title
   (:inherit 'dag/title-font :foreground dag-fg :height 200 :weight 'bold))
  (org-document-info-keyword
   (:foreground dag-fg2 :background dag-bg))
  (org-code
   (:inherit 'fixed-pitch :foreground dag-fg))
  (org-document-info
   (:foreground dag-fg))
  (org-meta-line
   (:foreground dag-fg2))
  (org-drawer
   (:inherit 'fixed-pitch :foreground dag-fg))
  (org-headline-done
   (:foreground dag-fg))
  (org-ellipsis
   (:inherit 'dag/ellipsis-font :background dag-bg))
  (org-dispatcher-highlight
   (:foreground dag-fg :background dag-green))
  (org-latex-and-related
   (:foreground dag-fg))
  (org-block-begin-line
   (:background dag-bg2 :extend t))
  (org-block-end-line
   (:background dag-bg2 :extend t))
  (org-special-keyword
   (:foreground dag-fg))
  (org-drawer
   (:foreground dag-fg))
  (org-hide
   (:foreground dag-bg :background dag-bg))
  (org-checkbox
   (:inherit 'dag/title-font :height 180))
  (org-indent
   (:inherit ('fixed-pitch 'org-hide)))

  (outline-8
   (:inherit 'dag/title-font :weight 'normal))
  (outline-7
   (:inherit 'outline-8 :height 1.0))
  (outline-6
   (:inherit 'outline-8 :height 1.0))
  (outline-5
   (:inherit 'outline-8 :height 1.1))
  (outline-4
   (:inherit 'outline-8 :height 1.4))
  (outline-3
   (:inherit 'outline-8 :height 1.7 ))
  (outline-2
   (:inherit 'outline-8 :height 2.0 :weight 'medium))
  (outline-1
   (:inherit 'outline-8 :height 2.3 :weight 'heavy))

  ;; agenda
  (org-agenda-date-today
   (:foreground dag-fg :weight 'bold))
  (org-agenda-date-weekend
   (:foreground dag-fg))
  (org-agenda-date
   (:foreground dag-fg))
  (org-agenda-structure
   (:foreground dag-fg :weight 'bold))
  (org-agenda-calendar-event
   (:foreground dag-fg))
  (org-agenda-done
   (:foreground dag-fg))
  (org-time-grid
   (:foreground dag-fg))
  (org-agenda-clocking
   (:background dag-bg2 :foreground dag-fg :slant 'italic))
  (secondary-selection
   (:background dag-red :foreground dag-fg))

  ;; modeline
  (mode-line
   (:foreground dag-fg :background dag-bg2))
  (mode-line-inactive
   (:inherit 'mode-line))
  (doom-modeline-spc-face
   (:inherit 'mode-line))
  (doom-modeline-evil-insert-state
   (:inherit 'mode-line))
  (doom-modeline-evil-normal-state
   (:inherit 'mode-line))
  (doom-modeline-evil-visual-state
   (:inherit 'mode-line))
  (doom-modeline-buffer-modified
   (:inherit 'mode-line :foreground dag-dark_red))
  (doom-modeline-bar
   (:foreground dag-fg :background dag-fg))
  (doom-modeline-bar-inactive
   (:foreground dag-bg2 :background dag-bg2))
  (doom-modeline-info
   (:foreground dag-fg))
  (doom-modeline-project-dir
   (:foreground dag-fg :weight 'normal))
  (doom-modeline-buffer-path
   (:foreground dag-fg :weight 'normal))
  (doom-modeline-buffer-file
   (:foreground dag-fg :weight 'normal))
  (doom-modeline-buffer-major-mode
   (:foreground dag-fg :weight 'normal))
  (doom-modeline-warning
   (:inherit 'warning :weight 'bold))
  (doom-modeline-lsp-running
   (:inherit 'warning))
  (doom-modeline-lsp-warning
   (:inherit 'warning))
  (doom-modeline-lsp-success
   (:foreground dag-fg))
  (doom-modeline-notification
   (:inherit 'warning :weight 'bold))
  (doom-modeline-repl-warning
   (:inherit 'warning))
  (doom-modeline-repl-warning
   (:inherit 'warning))
  (doom-modeline-battery-warning
   (:inherit 'warning))

  ;; Misc
  (minibuffer-prompt
   (:background dag-bg :foreground dag-fg :box nil :weight 'normal))
  (rustic-question-mark
   (:background dag-yellow :foreground dag-fg))
  (orderless-match-face-0
   (:background dag-blue))
  (orderless-match-face-1
   (:background dag-green))
  (orderless-match-face-2
   (:background dag-yellow))
  (orderless-match-face-3
   (:background dag-red))
  (header-line
   (:background dag-bg))
  (flycheck-fringe-warning
   (:background dag-red :foreground dag-fg))
  (term-color-green
   (:foreground dag-dark_green))
  (term-color-red
   (:foreground dag-dark_red))
  (term-color-blue
   (:foreground dag-dark_blue))
  (term-color-cyan
   (:foreground dag-dark_blue))
  (term-color-cyan
   (:foreground dag-dark_blue))
  (term-color-black
   (:foreground dag-fg))
  (term-color-white
   (:foreground dag-bg))
  (term-color-yellow
   (:foreground dag-dark_yellow))
  (term-color-magenta
   (:foreground dag-dark_magenta))
  (cider-result-overlay-face
   (:background dag-bg2 :box dag-fg))
  (help-key-binding
   (:background dag-bg2 :foreground dag-fg :box nil :weight 'bold))

  ;; Company
  (company-scrollbar-bg
   (:background dag-bg2))
  (company-scrollbar-fg
   (:background dag-fg))
  (company-tooltip-common
   (:inherit 'company-tooltip))
  (company-tooltip-annotation
   (:foreground dag-fg :weight 'normal))
  (company-tooltip
   (:background dag-bg2 :foreground dag-fg :weight 'normal))


  ;; LSP
  (lsp-flycheck-info-unnecessary-face
   (:foreground dag-fg :underline nil))
  (lsp-ui-sideline-global
   (:foreground dag-fg2 :underline nil :height 0.5))

  ;; Nix
  (nix-antiquote-face
   (:foreground dag-fg :background dag-green))

  ;; Consult
  (consult-file
   (:weight 'normal))
  ))

(provide-theme 'alabaster-dag)
