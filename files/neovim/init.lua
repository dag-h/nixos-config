vim.g.mapleader = " "
vim.g.maplocalleader = ","
vim.opt.tabstop = 4;
vim.opt.softtabstop = 4;
vim.opt.shiftwidth = 4;
vim.opt.expandtab = true;
vim.opt.smartindent = true;
vim.opt.incsearch = true;
vim.opt.hlsearch = true;
vim.opt.autoindent = true
vim.opt.timeoutlen = 300 
vim.opt.scrolloff = 2
vim.opt.hidden = true
vim.opt.ignorecase = true
vim.opt.smartcase = true
vim.opt.splitright = true
vim.opt.splitbelow = true
vim.opt.incsearch = true
vim.opt.gdefault = true
vim.opt.relativenumber = true
vim.opt.number = true
vim.opt.wrap = true
vim.opt.termguicolors = true
vim.opt.cursorline = true
vim.opt.conceallevel = 3

vim.o.guifont = "JetBrains Mono:h12"

vim.opt.linespace = 8

vim.g.neovide_padding_top=20
vim.g.neovide_padding_left=20
vim.g.neovide_padding_right=20
vim.g.neovide_padding_bottom=20
vim.g.neovide_floating_shadow = false

vim.keymap.set('t', '<Esc>', '<C-\\><C-n>', { noremap = true })

vim.filetype.add({ extension = { purs = 'purescript' }})

vim.api.nvim_create_autocmd("FileType", {
  pattern = "purescript",
  callback = function()
    vim.opt_local.shiftwidth = 2
    vim.opt_local.softtabstop = 2
    vim.opt_local.expandtab = true
  end,
})
