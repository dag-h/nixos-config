local neogit = require('neogit')
neogit.setup {}
vim.keymap.set("n", "<leader>g", function() neogit.open({ kind = "auto" }) end, {})
