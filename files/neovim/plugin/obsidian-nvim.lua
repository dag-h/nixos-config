require("obsidian").setup({
  workspaces = {
    {
      name = "vault",
      path = "~/cloud/vault",
    }
  },
})
