local telescope = require('telescope')
telescope.load_extension('hoogle')

vim.api.nvim_create_autocmd("FileType", {
    pattern = "haskell",
    callback = function()
        vim.keymap.set('n', '<leader>fh', ":Telescope hoogle <CR>", opts)
    end
})
