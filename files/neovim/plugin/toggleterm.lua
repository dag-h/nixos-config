require("toggleterm").setup{
  open_mapping = [[<leader><CR>]],
  hide_numbers = true, -- hide the number column in toggleterm buffers
  insert_mappings = false;
  start_in_insert = true,
  shade_terminals = true,
  shading_factor = 0.8;
  direction = 'horizontal',
  close_on_exit = true, -- close the terminal window when the process exits
}
