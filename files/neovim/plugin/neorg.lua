require('neorg').setup {
  load = {
    ["core.defaults"] = {},
    ["core.concealer"] = {
        config = {
            icons = {
                todo = {
                    done = {
                        icon = "✓",
                    },
                    undone = {
                        icon = " ",
                    },
                },
            },
        },
    },
    ["core.completion"] = {
        config = {
            engine = "nvim-cmp",
        },
    },
    ["core.dirman"] = {
        config = {
            workspaces = {
                notes = "~/cloud/norg",
            },
        },
    },
    ["core.manoeuvre"] = {},
    ["core.export"] = {},
    ["core.export.markdown"] = {},
  }
}
