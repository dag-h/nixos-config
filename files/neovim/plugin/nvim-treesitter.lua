vim.filetype.add({extension = {wgsl = "wgsl", svelte = "svelte"}})

require('nvim-treesitter.configs').setup {
    sync_install = false,
    auto_install = false,
    highlight = {
        disable = { "rust" },
        enable = true,
        additional_vim_regex_highlighting = false;
    },
    autopairs = { enable = true },
    rainbow = {
        enable = true,
        extended_mode = true,
        max_file_lines = 2000,
    },
}
