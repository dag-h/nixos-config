local actions = require("telescope.actions")
local telescope = require('telescope.builtin')

require('telescope').setup({
    extensions = {
        hoogle = {
            render = 'treesitter'
        },
    },
    defaults = {
        mappings = {
            i = {
                ["<esc>"] = actions.close,
            },
        },
    },
})

vim.keymap.set("n", "<leader>ff", telescope.find_files, {})
vim.keymap.set("n", "<leader>fo", telescope.live_grep, {})
vim.keymap.set("n", "<leader>fb", telescope.buffers, {})
vim.keymap.set("n", "<leader>fg", telescope.git_files, {})
vim.keymap.set("n", "<leader>fs", telescope.lsp_dynamic_workspace_symbols, {})
