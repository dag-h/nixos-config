local lsp = require('lsp-zero')

lsp.on_attach(function(client, bufnr)
    local opts = {buffer = event.buf}
    vim.keymap.set('n', 'K', function() vim.lsp.buf.hover() end, opts)
    vim.keymap.set('n', 'gd', function() vim.lsp.buf.definition() end, opts)
    vim.keymap.set("n", "<F3>", function() vim.diagnostic.open_float() end, opts)
end)

lsp.skip_server_setup({'rust_analyzer'})

require('lspconfig').tsserver.setup{}
require('lspconfig').svelte.setup{}
require('lspconfig').eslint.setup{}

lsp.setup()
