{ pkgs, home-manager, lib, system, overlays, ... }:
with builtins;
{
  mkSystemUser = { name, groups }:
  {
    users.users."${name}" = {
        name = name;
        isNormalUser = true;
        isSystemUser = false;
        extraGroups = groups;
        shell = pkgs.zsh;
        initialPassword = "123";
    };
  };

  mkUser = { userName, userSettings, stateVersion }:
    let
      homeDir = "/home/${userName}";
    in
    {
      dag = userSettings;

      home = {
        username = userName;
        homeDirectory = homeDir;
        stateVersion = stateVersion;
        sessionPath = [ "${homeDir}/bin" "${homeDir}/.cargo/bin" ];
      };

      programs.firefox = {
        enable = true;
        profiles.dag = {
          userChrome = ''
            /* hides the native tabs */
            #TabsToolbar {
              visibility: collapse;
            }
          '';
          settings = {
            "toolkit.legacyUserProfileCustomizations.stylesheets" = true;
          };
        };
      };

      imports = [ ../modules/user ];
    };
}
