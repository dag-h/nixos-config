{ system, pkgs, inputs, home-manager, lib, user, ... }:
with builtins;

{
  mkHost = { userName, hostName, systemUser, NICs, availInitrdMods, kernelMods, kernelParams, kernelPackage ? null, wifi ? [], cpuCores, videoDrivers ? [], extra-modules, userSettings, systemSettings, stateVersion, specialArgs }:
    let
      # create a list with an attribute set for each network interface
      networkInterfaces = listToAttrs (map (n: {
        name = "${n}"; value = { useDHCP = true; };
      }) NICs);
      # system maintenance scripts
      upgrade = pkgs.writeScriptBin "upgrade" (builtins.readFile ../bin/upgrade.sh);
      update = pkgs.writeScriptBin "update" (builtins.readFile ../bin/update.sh);
      clean = pkgs.writeScriptBin "clean" (builtins.readFile ../bin/clean.sh);
      save = pkgs.writeScriptBin "save" (builtins.readFile ../bin/save.sh);
    in lib.nixosSystem {
      inherit system specialArgs;

      modules = [
        
        # home-manager used as module to get rid of `home-manager switch`
        home-manager.nixosModules.home-manager {
          home-manager = {
            useGlobalPkgs = true;
            useUserPackages = true;
            # create a home-manager configuration
            users."${userName}" = user.mkUser {
              inherit userName userSettings stateVersion;
            };
          };
        }

        # system configuration
        {
          dag = systemSettings;

          imports = [
            ../modules/system (user.mkSystemUser systemUser)
            "${specialArgs.nixpkgs-dag}/nixos/modules/programs/overwitch.nix"
          ];

          programs.overwitch = {
            enable = true;
          };

          nixpkgs.pkgs = pkgs;

          nix = {
            settings = {
              substituters = [
                "https://cache.nixos.org/"
                "https://nix-community.cachix.org"
              ];
              trusted-public-keys = [
                "nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs="
              ];
            };
            package = pkgs.nixUnstable;
            extraOptions = ''
              experimental-features = nix-command flakes
            '';
            settings = {
              max-jobs = lib.mkDefault cpuCores;
              auto-optimise-store = true;
            };
            # registry.nixpkgs.flake = inputs.nixpkgs;
            nixPath = [ "nixpkgs=${inputs.nixpkgs}" ];
          };

          time.timeZone = "Europe/Stockholm";

          users.defaultUserShell = pkgs.zsh;

          environment.systemPackages = with pkgs; [
            wget
            # firefox
            openssl
            cachix
            sqlite
            gcc
          ] ++ [
            update upgrade save clean
          ];

          networking = {
            hostName = "${hostName}";
            interfaces = networkInterfaces;
            useDHCP = false;
            wireless.interfaces = wifi;
            networkmanager.enable = true;
          };

          boot = {
            initrd.availableKernelModules = availInitrdMods;
            kernelModules = kernelMods;
            kernelParams = kernelParams;
            # kernelPackages = kernelPackage;
            kernelPackages = lib.mkIf (!builtins.isNull kernelPackage) kernelPackage;
          };

          services = {
            xserver = {
              videoDrivers = videoDrivers;
              xkb = {
                options = "eurosign:e";
                layout = "se";
              };
            };
            openssh = {
              enable = true;
              allowSFTP = true;
            };
            hoogle.enable = true;
          };

          hardware = {
            opengl = {
              enable = true;
              driSupport = true;
              driSupport32Bit = true;
            };
            enableRedistributableFirmware = true;
            keyboard.qmk.enable = true;
          };
          
          programs = {
            nix-ld.enable = true;
            zsh.enable = true;
          };

          i18n.defaultLocale = "en_US.UTF-8";
          i18n.inputMethod = {
            enabled = "ibus";
            ibus.engines = with pkgs.ibus-engines; [ libpinyin ];
          };

          console = {
            font = "Lat2-Terminus16";
          };

          system.stateVersion = stateVersion;
        }
      ] ++ extra-modules; 
    };
}
