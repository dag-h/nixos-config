{ pkgs, inputs, home-manager, system, lib, overlays, ... }:
rec {
  user = import ./user.nix {
    inherit pkgs home-manager lib system overlays;
  };
  host = import ./host.nix {
    inherit pkgs inputs home-manager lib system user;
  };
}
